<?php

require_once "../../../controllers/sales.controller.php";
require_once "../../../models/sales.model.php";

require_once "../../../controllers/clients.controller.php";
require_once "../../../models/clients.model.php";

require_once "../../../controllers/users.controller.php";
require_once "../../../models/users.model.php";

require_once "../../../controllers/products.controller.php";
require_once "../../../models/products.model.php";

class printBill{

public $code;

public function getBillPrinting(){

//WE BRING THE INFORMATION OF THE SALE

$itemSale = "code";
$valueSale = $this->code;
$order = "id";

$answerSale = SalesController::ctrShowSales($itemSale, $valueSale,$order);

$saledate = substr($answerSale["date"],0,-8);
$products = json_decode($answerSale["products"], true);
$netPrice =$answerSale["net_price"];
$tax = $answerSale["tax"];
$totalPrice = $answerSale["total"];

// //GET INFORMACIÓN OF THE CLIENT....

$itemClient = "id";
$valueClient = $answerSale["id_client"];

$answerClient = ControllerClients::ctrShowClients($itemClient, $valueClient);

//TRAEMOS LA INFORMACIÓN DEL Seller

$itemSeller = "id";
$valueSeller = $answerSale["id_seller"];

$answerSeller = UserController::ctrShowUsers($itemSeller, $valueSeller);

//REQUERIMOS LA CLASE TCPDF

require_once('tcpdf_include.php');

$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

$pdf->startPageGroup();

$pdf->AddPage('P', 'A6');

// ---------------------------------------------------------

$block1 = <<<EOF

	<table>
		
		<tr>
		<td style="background-color:white; width:40px">
				
		<div style="text-align:left; line-height:15px;">
			
			<br>
			<img src="images/receipt.jpg" width="50" height="50" style="float:left;">
		</div>

	</td>

	<td style="background-color:white; width:170px">

		<div style="text-align:center; line-height:15px;">					
			<span style="font-size:13.5px; background-color:#3c4172; color:#ffffff; margin-top:5px; padding-top:5px;">CASH SALE</span>
			<br>
			<span style="font-size:14.5px; font-weight:bold;">QUANTA-TECH</span><br>
			<span style="font-size:12.5px">AGENCIES LTD.</span> <br>
			<span style="font-size:7.5px" >Dealers in LED Lighting and Electricals Products</span>

		</div>
		
	</td>

		</tr>

	</table>

EOF;

$pdf->writeHTML($block1, false, false, false, false, '');

// ---------------------------------------------------------
$block1a = <<<EOF

	<table>
		
		<tr>
		<td style="background-color:white; width:140px">
				
		<div style="font-size:6.5px; text-align:left; line-height:8px;">
			P.O BOX 23318 - Nairobi-Kenya<br/>
			Tel: +254 746 446 133 / +254 727 295 898<br/>
			Dawan House, Ground Floor<br/>
			info@quantatechagencies.com<br/>
			https://quantatechagencies.com
		</div>

	</td>

	<td style="background-color:white; width:100px">

		<div style="font-size:8.5px; text-align:center; line-height:15px;">
			<span style="font-weight:bold;">Till Number 603297</span><br>					
			<span style="font-weight:bold;">Rept. No.: <span style="color:red;">$valueSale</span></span>					

		</div>
		
	</td>

		</tr>

	</table>

EOF;

$pdf->writeHTML($block1a, false, false, false, false, '');


// ---------------------------------------------------------

$nameUpClient= strtoupper($answerClient["name"]);
$nameUpSeller =strtoupper($answerSeller["name"]);
$block2 = <<<EOF

	<table style="font-size:7px; padding:5px 7px;">
	
		<tr>
		
			<td style="background-color:white; width:150px">

				<u>Customer: $nameUpClient</u>

			</td>

			<td style="background-color:white; width:90px; text-align:right">
			
				<u>Date: $saledate</u>

			</td>

		</tr>


	</table>

EOF;

$pdf->writeHTML($block2, false, false, false, false, '');

// ---------------------------------------------------------

$block3 = <<<EOF

	<table style="font-size:7px; padding:5px 7px;">

		<tr>
		
		<td style="border: 1px solid #0f0f6d; background-color:white; width:120px; text-align:center">Particular</td>
		<td style="border: 1px solid #0f0f6d; background-color:white; width:25px; text-align:center">Qty</td>
		<td style="border: 1px solid #0f0f6d; background-color:white; width:40px; text-align:center">@</td>
		<td style="border: 1px solid #0f0f6d; background-color:white; width:55px; text-align:center">Total(Ksh)</td>

		</tr>

	</table>

EOF;

$pdf->writeHTML($block3, false, false, false, false, '');

//---------------------------------------------------------

foreach ($products as $key => $item) {

$valueUnit =$item["price"];

$totalPrices = $item["totalPrice"];

$block4 = <<<EOF

	<table style="font-size:7px; padding:5px 7px;">

		<tr>
			
			<td style="border: 1px solid #0f0f6d; color:#333; background-color:white; width:120px; text-align:center">
				$item[description]
			</td>

			<td style="border: 1px solid #0f0f6d; color:#333; background-color:white; width:25px; text-align:center">
				$item[quantity]
			</td>

			<td style="border: 1px solid #0f0f6d; color:#333; background-color:white; width:40px; text-align:center">
				$valueUnit
			</td>

			<td style="border: 1px solid #0f0f6d; color:#333; background-color:white; width:55px; text-align:center">
				$totalPrices
			</td>
		</tr>
		
		

	</table>


EOF;

$pdf->writeHTML($block4, false, false, false, false, '');

}

// ---------------------------------------------------------

$block5 = <<<EOF

	<table style="font-size:7px; padding:5px 7px;">		


		<tr>
		
			<td style="border-right: 1px solid #0f0f6d; color:#333; background-color:white; width:100px; text-align:center"></td>

			<td style="border: 1px solid #0f0f6d; background-color:white; width:85px; text-align:center">
				Total
			</td>
			
			<td style="border: 1px solid #0f0f6d; color:#333; background-color:white; width:55px; text-align:center">
				 $totalPrice
			</td>
		</tr>


	</table>

EOF;

$pdf->writeHTML($block5, false, false, false, false, '');



// ---------------------------------------------------------
//SALIDA DEL ARCHIVO 

$pdf->Output('bill.pdf');

}

}

$bill = new printBill();
$bill -> code = $_GET["code"];
$bill -> getBillPrinting();

?>