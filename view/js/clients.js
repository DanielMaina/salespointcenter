/*========================================
=            ajaxEditCustomer            =
========================================*/


$(".table tbody").on("click","button.btnEditClient",function(){
	var idClient = $(this).attr("idClient");
	console.log("response " +idClient);

	var data = new FormData();
	data.append("idClient",idClient);

	$.ajax({
		url:"ajax/clients.ajax.php",
		method: "POST",
		data:data,
		cache:false,
		    contentType:false,
		    processData:false,
		    dataType: "json",
		success:function(redsponse){
			console.log(redsponse);
			$("#idClient").val(redsponse["id"]);
			$("#editName").val(redsponse["name"]);
			$("#editDocumentId").val(redsponse["document"]);
			$("#editEmail").val(redsponse["email"]);
			$("#editPhone").val(redsponse["phone"]);
			$("#editAddress").val(redsponse["address"]);
			$("#editDategoin").val(redsponse["lastlogin"]);
	
		}

	})
});
/*=====================================
=            DELETE CLIENT            =
=====================================*/
$(".btnDeleteClient").click(function (){
	var idClient =$(this).attr("idClient");
	swal({
	title:'Are you sure to delete this Client?',
	text:'If Not Sure Click Cancel!',
	showCancelButton: true,
	confirmButtonColor: '#3085d6',
	cancelButtonColor: '#d33',
	cancelButtonText:'Cancel',
    confirmButtonText:'Yes Delete',
  }).then((result)=> {
  	if(result.value) {
     window.location= "index.php?root=clients&idClient="+idClient;
  	}
  })
});