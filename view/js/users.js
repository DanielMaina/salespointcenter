/*=============================================
=            Upload imageSection            =
=============================================*/
$(".newPhoto").change(function(){
	var image= this.files[0];
	
	//validateType
	if (image["type"] != "image/jpeg" && image["type"] != "image/png") {
		$(".newPhoto").val("");

		swal({
			title: "Error while uploading Image",
			text:"Image type must be in jpeg or png",
			type:"error",
			confirmButtonText: "Cancel"
		});
	}else if(image["size"] > 2000000){
        
		swal({
			title: "Error while uploading Image",
			text:"Image size exceed threshold of 2mb re-try",
			type:"error",
			confirmButtonText: "Cancel"
		});
	}else{
		var dataImage= new FileReader;
		dataImage.readAsDataURL(image);

		$(dataImage).on("load", function(event){
			var rootImage = event.target.result;
			$(".previsualizer").attr("src",rootImage); 
		});
	} 

});


/*=============================================
=            EDIT USER            =
=============================================*/

$(document).on("click",".btnEditUser",function(){
	var idUser  = $(this).attr('idUser');
	
	var data= new FormData();
	data.append("idUser", idUser);
	$.ajax({
		url:"ajax/users.ajax.php",
		method: "POST",
		data:data,
		cache:false,
		contentType:false,
		processData:false,
		dataType: "json",
		success: function(response){
			console.log(response);
			
			$("#editName").val(response["name"]);
			$("#editUser").val(response["username"]);
			$("#editProfile").html(response["profile"]);
			$("#editProfile").val(response["profile"]);
			$("#currentPhoto").val(response["photo"]);
			$("#currentPassword").val(response["password"]);

			if (response["photo"] != "") {
				$(".previsualizer").attr("src", response["photo"]);
			}

		}
	});
});
/*======================================
=            ACTIVATE USERS            =
======================================*/

$(document).on("click",".btnActivate",function(){
	var userId = $(this).attr("userId");
	var userStatus = $(this).attr("userStatus");

	var  data = new FormData();
	data.append("activatId", userId);
	data.append("activatUser", userStatus);

	$.ajax({
		url: "ajax/users.ajax.php",
		method: "POST",
		data:data,
		cache:false,
		contentType:false,
		processData:false,
		success: function(response){
         if (window.matchMedia("(max-width:767px)").matches) {
         	swal({
			title: "User State updated Successfully",
			type:"success",
			confirmButtonText: "Ok!"
		}).then(function(result){
			if (result.value) {
				window.location = "users";
			}
		});
         }
		}
	});
	if (userStatus == 0) {
		$(this).removeClass('btn-success');
		$(this).addClass('btn-danger');
		$(this).html('Inactive');
		$(this).attr('userStatus', 1);
	}else{
		$(this).addClass('btn-success');
		$(this).removeClass('btn-danger');
		$(this).html('Active');
		$(this).attr('userStatus', 0);

	}
});
/*==============================================
=            CHECK UNIQUES USERNAME            =
==============================================*/
$("#newUser").change(function(){
	$(".alert").remove();
	var username=$(this).val();
	var data = new FormData();
	data.append("validateUsername", username);

	$.ajax({
		url: "ajax/users.ajax.php",
		method: "POST",
		data:data,
		cache:false,
		contentType:false,
		processData:false,
		dataType: "json",
		success: function(response){
        if (response) { 
        	$("#newUser").parent().after('<div class="alert alert-warning">Username Already used PLEASE RE-Try another</div>');
        	$("#newUser").val("");
        }
		}
	});

});
/*=============================================
=            Delete user            =
=============================================*/
$(document).on("click",".btnDeleteUser",function(){
	var idUser =$(this).attr("idUser");
	var userPhoto =$(this).attr("userPhoto");
	var username =$(this).attr("username");
	swal({
		title:'Are you sure you want to DELETE user?',
		text: 'If Not Sure you can Cancel the action',
		type:'warning',
		showCancelButton: true,
		confirmButtonColor:'#30885d6',
		cancelButtonColor: '#d33',
		cancelButtonText: 'Cancel',
		confirmButtonText:'YES'
	}).then((result)=>{
		if(result.value){
			window.location = "index.php?root=users&idUser="+idUser+"&username="+username+"&userphoto="+userPhoto;
		}
	})
});