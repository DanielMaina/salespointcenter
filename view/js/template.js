 /*=============================================
 =            SideBar menu           =
 =============================================*/

 $('.sidebar-menu').tree()

 /*=============================================
 =          MANIPULATE DATATABLE            =
 =============================================*/
 $(".tables").DataTable();

/*===========================================================
=            iCheck for checkbox and Radio input            =
===========================================================*/
$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
checkboxClass: 'icheckbox_minimal-blue',
radioClass : 'iradio_minimal-blue'
});

/*=================================
=            inputmask format           =
=================================*/

//Datemask dd/mm/yyyy
$('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
//Datemask2 mm/dd/yyyy
$('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
//Money Euro
$('[data-mask]').inputmask()