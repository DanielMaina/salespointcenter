/*==========================================
=   VARIABLE LOCAL STORAGE............  =
==========================================*/
if (localStorage.getItem("captureRange") != null) {
   $("#daterange-btn span").html(localStorage.getItem("captureRange"));
}else{
  $("#daterange-btn span").html('<i class="fa fa-calendar"></i> Date Range');
}

/*==========================================
=           LOAD DYNAMIC DATATABLE SALES  =
==========================================*/

// $.ajax({
// url: "ajax/datatable-sales.ajax.php",
// success:function(response){
// 	console.log("response", response);
// }
// });
$(".salesTable").DataTable({
	"ajax": "ajax/datatable-sales.ajax.php",
	"deferRender": true,
	"retrieve": true,
	"processing": true
});

/*============================================
=            ADD PRODUCT TO SALES            =
============================================*/

$(".salesTable").on("click","button.addProduct", function(){
	var idProduct = $(this).attr("idProduct");

	$(this).removeClass("btn-success addProduct");
	$(this).addClass("btn-default");
	var data = new FormData();
	data.append("idProduct", idProduct);
       $.ajax({
		url:"ajax/product.ajax.php",
		method: "POST",
		data:data,
		cache:false,
		contentType:false,
		processData:false,
		dataType: "json",
		success:function(response){
              var description = response["description"];
              var stock =response["stock"];
              var price=response["selling_price"];
              	if(stock == 0){

      			swal({
			      title: "There's no stock available",
			      type: "error",
			      confirmButtonText: "Close!"
			    });

			    
			    $("button[idProduct='"+idProduct+"']").addClass("btn-success addProduct");

			    return;

          	}

              $(".newProduct").append(
              	'<div class="row" style="padding:5px 15px">'+
                 '<div class="col-xs-5">'+
                   '<div class="input-group">'+
                     '<span class="input-group-addon">'+
                     '<button class="btn btn-danger btn-xs removeProduct" idProduct="'+idProduct+'"><i class="fa fa-times"></i></button></span>'+
                     '<input type="text" class="form-control newDescriptionProduct" name="addProduct" value="'+ description +'" idProduct="'+idProduct+'" readonly required>'+
                   '</div>'+
                 '</div>'+
                   '<div class="col-xs-2" style="padding-left:0px;">'+
                   '<input type="number" class="form-control newProductQuantity" name="newProductQuantity" value="1" min="1" stock="'+stock+'" newStock="'+Number(stock-1) +'" required>'+
                 '</div>'+ 
                 '<div class="col-xs-5 sellingPrice" style="padding-left:0px;">'+
                  '<div class="input-group">'+
                   '<span class="input-group-addon">Ksh</span>'+
                   '<input type="number" class="form-control newProductPrice1" realPrice1="'+price+'" name="newProductPrice1" value="'+price+'" required>'+
                 '</div>'+
                 '</div>'+
                 '<div class="col-xs-5 sellingPrice" style="padding-left:0px; margin-top:1px;">'+
                  '<div class="input-group">'+
                   '<span class="input-group-addon">Ksh</span>'+
                   '<input type="text" class="form-control newProductPrice" realPrice="'+price+'" name="newProductPrice" value="'+price+'" required readonly>'+
                 '</div>'+
                 '</div>'+
                 '</div>'
               );
              // func to addTotalPrice and addTax...
              addTotalPrices();
              addTax();
              //total product in jsonformat
              listProducts();

              //format price 
              $(".newProductPrice").number(true, 2); 
		}
	});
});
/*========================================================
=            CONTROL WHILE NAVIGATING TABLE            =
========================================================*/

$(".salesTable").on("draw.dt", function(){
 if (localStorage.getItem("removeProduct") != null) {
 	var listProducts = JSON.parse(localStorage.getItem("removeProduct"));
 	for (var i =0; i< listProducts.length; i++) {
 		$("button.recoverButton[idProduct='"+listProducts[1]["idProduct"]+"'] ").removeClass('btn-default');
 		$("button.recoverButton[idProduct='"+listProducts[1]["idProduct"]+"'] ").addClass('btn-success addProduct');
 		     // func to addTotalPrice...
              addTotalPrices();
              addTax();
            //total product in jsonformat
              listProducts();
 	}
 }
});

/*========================================================
=            DELETE FROM SALES RECOVERBUTTON            =
========================================================*/

var idRemoveProduct =[];
localStorage.removeItem("removeProduct");
$(".formSales").on("click","button.removeProduct", function(){

$(this).parent().parent().parent().parent().remove();
var idProduct= $(this).attr("idProduct");
//storeId to local storage
if (localStorage.getItem('removeProduct') == null) {
	idRemoveProduct =[];
}else{
	idRemoveProduct.concat(localStorage.getItem("removeProduct"));
}
	idRemoveProduct.push({"idProduct":idProduct});
	localStorage.setItem("removeProduct", JSON.stringify(idRemoveProduct));

$("button.recoverButton[idProduct='"+idProduct+"']").removeClass("btn-default");
$("button.recoverButton[idProduct='"+idProduct+"']").addClass("btn-success addProduct");
//method to control the behavour of Delete all product {remove by the click}
   if ($(".newProduct").children().length == 0 ) {
   	$("#newSellerTax").val(0);
   	$("#newSellerPrice").val(0);
    $("#totalSale").val(0);
   	$("#newSellerPrice").attr("total",0);
   }else{
  		     // func to addTotalPrice...
              addTotalPrices();
              addTax();
            //total product in jsonformat
              listProducts();
   }
})

/*----------ADD PRODUT CODE FOR THE MOBLE DEVICES----------*/
 var numProduct = 0;
$(".btnAddProduct").click(function(){

	numProduct ++;
	var data = new FormData();
	data.append("bringProduct","ok");

	$.ajax({
		url:"ajax/product.ajax.php",
		method: "POST",
		data:data,
		cache:false,
		contentType:false,
		processData:false,
		dataType: "json",
		success:function(response){
              $(".newProduct").append(
              	'<div class="row" style="padding:5px 15px">'+
                 '<div class="col-xs-5">'+
                   '<div class="input-group">'+
                     '<span class="input-group-addon">'+
                     '<button class="btn btn-danger btn-xs removeProduct" idProduct><i class="fa fa-times"></i></button></span>'+
                    '<select class="form-control newDescriptionProduct" idProduct  id="product'+numProduct+'"  name="newDescriptionProduct" required>'+
                        '<option>Select Product </option>'+
                     '</select>'+
                   '</div>'+
                 '</div>'+
                   '<div class="col-xs-2 newStock" style="padding-left:0px;">'+
                  
                   '<input type="number" class="form-control newProductQuantity" name="newProductQuantity" value="1" stock newStock min="1" required>'+
                 '</div>'+ 
                 '<div class="col-xs-5 sellingPrice" style="padding-left:0px;">'+
                  '<div class="input-group">'+
                   '<span class="input-group-addon">Ksh</span>'+
                   '<input type="text" class="form-control newProductPrice" realPrice="" name="newProductPrice" required readonly>'+
                 '</div>'+
                 '</div>'+
                 '</div>'
               );
              // LOOP DB AND GET THE PROD. DESCRIPTION.....
              response.forEach(functionForEach);
              function functionForEach(item, index){
              	if (item.stock != 0 ){
              	$("#product"+numProduct).append(
                     '<option idProduct="'+item.id+'" value="'+item.description+'">'+item.description+'</option>'
              		)
              }
          }
           // func to addTotalPrice...
              addTotalPrices();
              addTax();
              //format price 
              $(".newProductPrice").number(true, 2);
              
	    }
	})
});
              // LOOP DB AND GET THE PROD. PRICE.....{for small device}

$(".formSales").on("click","select.newDescriptionProduct", function(){
	var nameProduct = $(this).val();
	var newDescriptionProduct = $(this).parent().parent().parent().children().children().children(".newDescriptionProduct");//27-04-errorHandler
	var newProductPrice = $(this).parent().parent().parent().children(".sellingPrice").children().children(".newProductPrice");
	var newProductQuantity = $(this).parent().parent().parent().children(".newStock").children(".newProductQuantity");
	var data = new FormData();
	data.append("nameProduct",nameProduct);

	$.ajax({
		url:"ajax/product.ajax.php",
		method: "POST",
		data:data,
		cache:false,
		contentType:false,
		processData:false,
		dataType: "json",
		success:function(response){
		$(newDescriptionProduct).attr("idProduct", response["id"]); //27-04-errorHandler    
		$(newProductQuantity).attr("stock", response["stock"]);
		$(newProductQuantity).attr("newStock",Number(response["stock"]-1));
        $(newProductPrice).val(response["selling_price"]);
        $(newProductPrice).attr("realPrice", response["selling_price"]); //27-04-errorHandler
        
    // func to addTotalPrice...27-04-errorHandler
       addTotalPrices();
       addTax();    
    //total product in jsonformat
      listProducts();
		}
    })
})
/**
 * MODIFY PRICE AND UPDATE VALUES
 * 
 */
$(".formSales").on("change","input.newProductPrice1",function(){
var price=$(this).parent().parent().parent().children(".sellingPrice").children().children(".newProductPrice");

price.val($(this).val());
 price.attr("realPrice",$(this).val());

var currentQty=$(this).parent().parent().parent().children().children(".newProductQuantity");
var newSellSbuT=currentQty.val()*price.val();
price.val(newSellSbuT);



    // func to addTotalPrice...
    addTotalPrices();
    addTax();
     //total product in jsonformat
    listProducts();
})
/*========================================================
=            MODIFY THE QUANTITY AND PRICE       =
========================================================*/
$(".formSales").on("change","input.newProductQuantity", function(){
  var price = $(this).parent().parent().children(".sellingPrice").children().children(".newProductPrice");
 var finalPrice = $(this).val() * price.attr("realPrice");
  price.val(finalPrice);
  var newStock=($(this).attr("stock")) - $(this).val();
  $(this).attr("newStock", newStock);

  if(Number($(this).val()) > Number($(this).attr("stock")) ) {
  	//handle field once product removed...
  	$(this).val(1);

  	var finalPrice = $(this).val() * price.attr("realPrice");
  	price.val(finalPrice);
  	addTotalPrices();

  	  swal({
		title: "Sorry, Insurfficient Stock",
		text: "Stock "+$(this).attr("stock")+" units",
		type: "error",
		confirmButtonText: "Close!"
		 });
  }
                // func to addTotalPrice...
              addTotalPrices();
              addTax();
               //total product in jsonformat
              listProducts();
})
/*=============================================
=            function to add total prices...            =
=============================================*/
function addTotalPrices(){
	 var itemPrice=$(".newProductPrice");
	 var arraySumPrice=[];
	 for (var i = 0; i < itemPrice.length; i++) {
	 	arraySumPrice.push(Number($(itemPrice[i]).val()));
	 }
	 function sumPriceArray(total, number) {
	 	return total + number;
	 }
	 var sumPriceArray = arraySumPrice.reduce(sumPriceArray);
	 $("#newSellerPrice").val(sumPriceArray);
   $("#totalSale").val(sumPriceArray);
	 $("#newSellerPrice").attr("total",sumPriceArray);
}

/*=====================================================
=            HADLE THE PRODUCT TAXATION...            =
=====================================================*/

function addTax(){
	var tax = $("#newSellerTax").val();
	var totalPrice =$("#newSellerPrice").attr("total");
	var taxPrice= Number(totalPrice * tax/100);
	var totalWithTax=Number(taxPrice) + Number(totalPrice);
    $("#newSellerPrice").val(totalWithTax);
    $("#totalSale").val(totalWithTax);
    $("#newPriceTax").val(taxPrice);
    $("#newNetPrice").val(totalPrice);
}
/*=========================================================
=            Change UI once taxInput changes            =
=========================================================*/

$("#newSellerTax").change(function(){
	addTax(); 
});
/*----------  handle number format total price ----------*/

 $("#newSellerPrice").number(true, 2);

 /*============================================
 =            Payment method SELECT!!!!!!            =
 ============================================*/

 $("#newPaymentMethod").change(function(){
  var method = $(this).val();
  if (method == "Cash"){
    $(this).parent().parent().removeClass("col-xs-6");
    $(this).parent().parent().addClass("col-xs-3");

    $(this).parent().parent().parent().children(".paymentMethodBoxes").html(
     '<div class="col-xs-5">'+
     '<div class="input-group">'+
     '<input type="text" class="form-control" id="newCashValue" placeholder="000000" required>'+
     '</div>'+
     '</div>'+
     '<div class="col-xs-4" id="getCashChange" style="padding-left:0px">'+

        '<div class="input-group">'+


          '<input type="text" class="form-control" id="newCashChange" placeholder="000000" readonly  required>'+

        '</div>'+

       '</div>'
      );

    // Adding format to the price
    $('#newCashValue').number( true, 2);
    $('#newCashChange').number( true, 2);
    //paymeny using cash
    listMethods();

  }else if(method=="MP"){
    $(this).parent().parent().removeClass('col-xs-4');

    $(this).parent().parent().addClass('col-xs-6');
         $(this).parent().parent().parent().children('.paymentMethodBoxes').html(
      '<div class="col-xs-6" style="padding-left:0px">'+
                        
                '<div class="input-group">'+
                     
                  '<input type="text" class="form-control" id="newTransactionCode" placeholder="M-Pesa receipt No.">'+
                       
                  '<span class="input-group-addon"><i class="fa fa-lock"></i></span>'+
                  
                '</div>'+

              '</div>');

  }
  else if(method=="CQ"){
    $(this).parent().parent().removeClass('col-xs-4');

    $(this).parent().parent().addClass('col-xs-6');
         $(this).parent().parent().parent().children('.paymentMethodBoxes').html(
      '<div class="col-xs-6" style="padding-left:0px">'+
                        
                '<div class="input-group">'+
                     
                  '<input type="text" class="form-control" id="newTransactionCode" placeholder="Cheque Number">'+
                       
                  '<span class="input-group-addon"><i class="fa fa-lock"></i></span>'+
                  
                '</div>'+

              '</div>');

  }
  else if(method=="IN"){
    $(this).parent().parent().removeClass('col-xs-4');

    $(this).parent().parent().addClass('col-xs-6');
         $(this).parent().parent().parent().children('.paymentMethodBoxes').html(
      '<div class="col-xs-6" style="padding-left:0px">'+
                        
                '<div class="input-group">'+
                     
                  '<input type="text" class="form-control" id="newTransactionCode" placeholder="Invoice Number">'+
                       
                  '<span class="input-group-addon"><i class="fa fa-lock"></i></span>'+
                  
                '</div>'+

              '</div>');

  }
 });

 /*=============================================
CASH CHANGE
=============================================*/
$(".formSales").on("change", "input#newCashValue", function(){
  var cash = $(this).val();
  var change =  Number(cash) - Number($('#newSellerPrice').val());
  var newCashChange = $(this).parent().parent().parent().children('#getCashChange').children().children('#newCashChange');

  newCashChange.val(change);
  })

 /*=============================================
CASH TRANSACTION
=============================================*/
$(".formSales").on("change", "input#newTransactionCode", function(){

   //paymeny using cash
    listMethods();
     
  })

/*=============================================
LIST ALL THE PRODUCTS
=============================================*/

function listProducts(){

  var productsList = [];

  var description = $(".newDescriptionProduct");

  var quantity = $(".newProductQuantity");

  var price = $(".newProductPrice");
  for(var i = 0; i < description.length; i++){
    productsList.push({ "id" : $(description[i]).attr("idProduct"), 
                "description" : $(description[i]).val(),
                "quantity" : $(quantity[i]).val(),
                "stock" : $(quantity[i]).attr("newStock"),
                "price" : $(price[i]).attr("realPrice"),
                "totalPrice" : $(price[i]).val()})
  }
 

  $("#productsList").val(JSON.stringify(productsList));
  console.log(JSON.stringify(productsList));

}
/*==========================================
=            GET PAYMENT METHOD            =
==========================================*/

function listMethods(){

  var listMethods = "";

  if($("#newPaymentMethod").val() == "Cash"){

    $("#listPaymentMethod").val("Cash");

  }else{

    $("#listPaymentMethod").val($("#newPaymentMethod").val()+"-"+$("#newTransactionCode").val());

  }

}
/*=======================================
=            ButtonEditSales            =
=======================================*/
// $(".table").on("click",".btnPrintBill", function(){
$(".table").on("click",".btnEditSales", function(){  
// $(".btnEditSales").click(function(){
  var idSales =$(this).attr("idSales");
  window.location = "index.php?root=edit-sales&idSales="+idSales;
});

/*=============================================
DELETE SALE
=============================================*/
$(".table").on("click",".btnDeleteSale", function(){
// $(".btnDeleteSale").click(function(){

  var idSales = $(this).attr("idSales");

  swal({
        title: 'Are you sure you want to delete the sale?',
        text: "If you're not you can cancel!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancel',
        confirmButtonText: 'Yes, delete sale!'
      }).then(function(result){
        if (result.value) {
          
        window.location ="index.php?root=sales&idSales="+idSales;
        }

  })

})

/*==================================
=            PrintBilll            =
==================================*/
$(".table").on("click",".btnPrintBill", function(){
  var saleCode = $(this).attr("saleCode");
  var payMethod=$(this).attr("paymethod");
  var result = payMethod.substring(0, 2);
  if(result=="IN"){
    window.open("extensions/tcpdf/pdf/invoice.php?code="+saleCode,"_blank");
  }else{
    window.open("extensions/tcpdf/pdf/bill.php?code="+saleCode,"_blank");
  }
  
})

/*==================================
= 27-11-2020        PrintDebtPayment           =
==================================*/
$(".table").on("click",".btnPrintDebtPayment", function(){
  var saleCode = $(this).attr("saleCode");
    window.open("extensions/tcpdf/pdf/debtTracker.php?code="+saleCode,"_blank");
  
  
})
/*=============================================
DATES RANGE
=============================================*/

$('#daterange-btn').daterangepicker(
  {
    ranges   : {
      'Today'       : [moment(), moment()],
      'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
      'Last 7 days' : [moment().subtract(6, 'days'), moment()],
      'Last 30 days': [moment().subtract(29, 'days'), moment()],
      'this month'  : [moment().startOf('month'), moment().endOf('month')],
      'Last month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    },
    startDate: moment(),
    endDate  : moment()
  },
  function (start, end) {
    $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));

    var initialDate = start.format('YYYY-MM-DD');

    var finalDate = end.format('YYYY-MM-DD');

    var captureRange = $("#daterange-btn span").html();
   
    localStorage.setItem("captureRange", captureRange);
    window.location = "index.php?root=sales&initialDate="+initialDate+"&finalDate="+finalDate;

  }

)
/*=========================================
=            CANCEL DATE RANGE  .opensleft           =
=========================================*/
$(".daterangepicker.opensleft .range_inputs .cancelBtn").on("click", function(){

  localStorage.removeItem("captureRange");
  localStorage.clear();
  window.location = "sales";

})

/*=============================================
CAPTURE TODAY'S BUTTON 
=============================================*/

$(".daterangepicker.opensleft .ranges li").on("click", function(){

  var todayButton = $(this).attr("data-range-key");
  

  if(todayButton == "Today"){

    var d = new Date();
    
    var day = d.getDate();
    var month= d.getMonth()+1;
    var year = d.getFullYear();

    if((month < 10) && (day < 10)){
      var initialDate = year+"-0"+month+"-0"+day;
      var finalDate = year+"-0"+month+"-0"+day;

    }else if(day < 10){

      var initialDate = year+"-"+month+"-0"+day;
      var finalDate = year+"-"+month+"-0"+day;

    }else if(month < 10){

      var initialDate = year+"-0"+month+"-"+day;
      var finalDate = year+"-0"+month+"-"+day;
    }else{

      var initialDate = year+"-"+month+"-"+day;
        var finalDate = year+"-"+month+"-"+day;

    } 

      localStorage.setItem("captureRange", "Today");

      window.location = "index.php?root=sales&initialDate="+initialDate+"&finalDate="+finalDate;
     
  }

})


/*=============================================
BUTTON UPDATE DEPT PAYMENT 
=============================================*/
$(".table").on("click",".btnPayDebt", function(){
let receiptNo=$(this).attr("receiptNo");
// console.log(receiptNo);
$("#receiptNo").val(receiptNo);
})


/*=============================================
REQUEST TO PREVENT REPETATION OF R/I NO 
=============================================*/
$("#newSales").change(function(){
  $(".alert").remove();
  let newSales=$(this).val();
  let data=new FormData();
  console.log(newSales);
  data.append("validateRINo",newSales);
  $.ajax({
    url:"ajax/sales.ajax.php",
    method:"POST",
    data:data,
    cache:false,
    contentType:false,
    processData:false,
    dataType:"json",
    success: function(validateResult){
      if(validateResult){
        $("#newSales").parent().before('<div class="alert alert-danger"><b>Error!</b> Invoice/Receipt Number already exist. Contact Admin</div>');
           $("#newSales").val("");
      }
    }
  })
})
