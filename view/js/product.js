/*==========================================
=            LOAD DYNAMIC DATATABLE            =
==========================================*/

// $.ajax({
// // url: "ajax/datatable-products.ajax.php",
// // success:function(response){
// // 	console.log("response", response);
// // }
// // });
var hiddenProfile= $("#hiddenProfile").val();
$(".tableProducts").DataTable({
	"ajax": "ajax/datatable-products.ajax.php?hiddenProfile="+hiddenProfile,
	"deferRender": true,
	"retrieve": true,
	"processing": true
});

$("#newCategory").change(function(){
	var idCategory = $(this).val();
	var data = new FormData();
	data.append("idCategory", idCategory);

	$.ajax({
		url:"ajax/product.ajax.php",
		method: "POST",
		data:data,
		cache:false,
		contentType:false,
		processData:false,
		dataType: "json",
		success:function(response){
			if (!response) {
				var newCode=idCategory+ "01";
				$("#newCode").val(newCode);
			}else{
			var newCode=Number(response["code"]) + 1;
			$("#newCode").val(newCode);
		   }
		}
	})

});
/*===========================================
=            AddingSellingPPrice            =
===========================================*/
$("#newBuyingPrice, #editBuyingPrice").change(function(){
	if ($(".percentage").prop("checked")){
 var valuePercentage = $(".newPercentage").val();

 var percentage = Number(($("#newBuyingPrice").val()*valuePercentage/100))+Number($("#newBuyingPrice").val());

  var editPercentage = Number(($("#editBuyingPrice").val()*valuePercentage/100))+Number($("#editBuyingPrice").val());
 
 $("#newSellingPrice").val(percentage);
 $("#newSellingPrice").prop("readonly",true);

  $("#editSellingPrice").val(editPercentage);
 $("#editSellingPrice").prop("readonly",true);
 }	
});
/*=========================================
=            CHANGE PERCENTAGE            =
=========================================*/

$(".newPercentage").change(function (){
if ($(".percentage").prop("checked")){
 var valuePercentage = $(this).val();

 var percentage = Number(($("#newBuyingPrice").val()*valuePercentage/100))+Number($("#newBuyingPrice").val());
 
 var editPercentage = Number(($("#editBuyingPrice").val()*valuePercentage/100))+Number($("#editBuyingPrice").val());

 $("#newSellingPrice").val(percentage);
 $("#newSellingPrice").prop("readonly",true);

   $("#editSellingPrice").val(editPercentage);
 $("#editSellingPrice").prop("readonly",true);
 }	
});
/*========================================
=            UNCHECK CHECKBOK            =
========================================*/
$(".percentage").on("ifUnchecked", function(){
$("#newSellingPrice").prop("readonly",false);
$("#editSellingPrice").prop("readonly",false);
});
$(".percentage").on("ifChecked", function(){
$("#newSellingPrice").prop("readonly",true);
$("#editSellingPrice").prop("readonly",true);
});

/*=============================================
=            Upload imageSection            =
=============================================*/
$(".newImage").change(function(){
	var image= this.files[0];
	
	//validateType
	if (image["type"] != "image/jpeg" && image["type"] != "image/png") {
		$(".newImage").val("");

		swal({
			title: "Error while uploading Image",
			text:"Image type must be in jpeg or png",
			type:"error",
			confirmButtonText: "Cancel"
		});
	}else if(image["size"] > 2000000){
       $(".newImage").val(""); 
		swal({
			title: "Error while uploading Image",
			text:"Image size exceed threshold of 2mb re-try",
			type:"error",
			confirmButtonText: "Cancel"
		});
	}else{
		var dataImage= new FileReader;
		dataImage.readAsDataURL(image);

		$(dataImage).on("load", function(event){
			var rootImage = event.target.result;
			$(".previsualizer").attr("src",rootImage); 
		});
	} 

});
/*====================================
=            EDIT PRODUCT            =
====================================*/
$(".tableProducts tbody").on("click", "button.btnEditProduct",function(){
var idProduct=$(this).attr("idProduct");
var data = new FormData();
data.append("idProduct", idProduct);

$.ajax({
		url:"ajax/product.ajax.php",
		method: "POST",
		data:data,
		cache:false,
		contentType:false,
		processData:false,
		dataType: "json",
		success:function(response){
			var dataCategory = new FormData();
			dataCategory.append("idCategory",response["id_category"]);
			$.ajax({
		    url:"ajax/category.ajax.php",
		    method: "POST",
		    data:dataCategory,
		    cache:false,
		    contentType:false,
		    processData:false,
		    dataType: "json",
		    success:function(response){
		    	$("#editCategory").val(response["id"]);
		    	$("#editCategory").html(response["category"]);
              }
	   })
	$("#editId").val(response["id"]);   
	$("#editSales").val(response["sales"]);   
	$("#editCode").val(response["code"]);		   
	$("#editDescription").val(response["description"]);		   
	$("#editStock").val(response["stock"]);		   
	$("#editBuyingPrice").val(response["buying_price"]);		   
	$("#editSellingPrice").val(response["selling_price"]);
	
	if(response["image"] != "") {
		   
	$("#actualImage").val(response["image"]);	
	$(".previsualizer").attr("src", response["image"]);
	}

  }
		
 });	

}); 
/*====================================
=            DELETE PRODUCT            =
====================================*/
$(".tableProducts tbody").on("click", "button.btnDeleteProduct",function(){
var idProduct=$(this).attr("idProduct");
var code=$(this).attr("code");
var image=$(this).attr("image");
swal({
	title:'Are you sure to delete this product?',
	text:'If Not Sure Click Cancel!',
	showCancelButton: true,
	confirmButtonColor: '#3085d6',
	cancelButtonColor: '#d33',
	cancelButtonText:'Cancel',
    confirmButtonText:'Yes Delete',
  }).then((result)=> {
  	if(result.value) {
     window.location= "index.php?root=products&idProduct="+idProduct+"&image="+image+"&code="+code;
  	}
  })

})