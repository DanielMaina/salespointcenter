/*=====================================
=            EDIT CATEGORY            =
=====================================*/
$(".btnEditCategory").click(function(){
	var idCategory = $(this).attr("idCategory");
	var data = new FormData();
	data.append("idCategory", idCategory);
	$.ajax({
		url:"ajax/category.ajax.php",
		method: "POST",
		data:data,
		cache:false,
		contentType:false,
		processData:false,
		dataType: "json",
		success: function(response){
			$("#editCategory").val(response["category"]);
			$("#idCategory").val(response["id"]);
			
		}
	});
});
/*======================================
=            DELETECATEGORY            =
======================================*/
$(".btnDeleteCategory").click(function(){
	var idCategory =$(this).attr("idCategory");

	 swal({
			 	
                text: 'Do you really want to delete!',
                showCancelButton: true,
                confirmButtonColor:'#3085d6',
                cancelButtonColor:'#d33',
                cancelButtonText:'Cancel',
                confirmButtonText:'Yes'
			 	}).then((result)=>{
			 		if(result.value){
			 			window.location = 'index.php?root=categories&idCategory='+idCategory;
			 		}
			 		});
});
