<?php 
if ($_SESSION["profile"] == "seller") {
echo '
  <script>
window.location = "home";
  </script>  
 ';
 return;
}
   ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Quanta Manage Category Dashboard
      </h1>
      <ol class="breadcrumb">
        <li><a href="home"><i class="fa fa-dashboard"></i>Home</a></li>
        <li class="active">Category Management</li>
      </ol>
    </section>

  
    <section class="content">

      
      <div class="box">
        <div class="box-header with-border">
<button class="btn btn-primary" data-toggle="modal" data-target="#addCategory">Add Category</button>
         
</div>
<div class="box-body">
<table class="table table-bordered table-striped dt-responsive tables ">
 <thead>
 <tr>
 <th style="width: 10px;">#</th> 
  <th>Category</th> 
  <th>Actions</th>  
 </tr> 
 </thead> 
 <tbody>
  <?php 
  $item=null;
  $value=null;      
  $categories=CategoryController::crtShowCategory($item, $value);
  foreach ($categories as $key => $value) {
    echo '<tr>
  <td>'.($key+1).'</td> 
  <td class="text-uppercase">'.$value["category"].'</td> 
  <td>
    <div class="btn-group">
      <button class="btn btn-warning btnEditCategory" idCategory="'.$value["id"].'" data-toggle="modal" data-target="#modalEditCategory"><i class="fa fa-pencil"></i></button>';
      if ($_SESSION["profile"]== "administrator") {
    
      echo '
      <button class="btn btn-danger btnDeleteCategory" idCategory="'.$value["id"].'"><i class="fa fa-times"></i></button>';
        }
    echo '</div>

  </td> 
 </tr> ';
  }


   ?>
  <?php 
$deleteCategory= new CategoryController();
$deleteCategory-> ctrDeleteCategory();

   ?>
  
 </tbody>
</table>        
</div>
       
 </div>
</section>
 
</div>
 
<!-- THE MODAL add category -->

<!-- Modal -->
<div id="addCategory" class="modal fade" role="dialog">

  <div class="modal-dialog">

   

    <div class="modal-content">

      <form role="form" method="POST">

        <!--=====================================
        HEADER
        ======================================-->

        <div class="modal-header" style="background: #3c8dbc; color: #fff">

          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title">Add Category</h4>

        </div>

        <!--=====================================
        BODY
        ======================================-->

        <div class="modal-body">

          <div class="box-body">

            <!--Input name -->

            <div class="form-group">

              <div class="input-group">

                <span class="input-group-addon"><i class="fa fa-th"></i></span>

                <input class="form-control input-lg" type="text" name="newCategory" placeholder="Add Category" required>

              </div>

            </div>

          </div>

        </div>

        <!--=====================================
        FOOTER
        ======================================-->

        <div class="modal-footer">

          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>

          <button type="submit" name="addCategory" class="btn btn-primary">Add Category</button>

        </div>
        <?php 
$createCategory= new CategoryController();
$createCategory-> ctrCreateCategory();
         ?>

      </form>

    </div>

  </div>

</div>

 
<!-- THE MODAL EDIT CATEGORY~~~ -->

<!-- Modal -->
<div id="modalEditCategory" class="modal fade" role="dialog">

  <div class="modal-dialog">

   

    <div class="modal-content">

      <form role="form" method="POST">

        <!--=====================================
        HEADER
        ======================================-->

        <div class="modal-header" style="background: #3c8dbc; color: #fff">

          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title">EDIT Category</h4>

        </div>

        <!--=====================================
        BODY
        ======================================-->

        <div class="modal-body">

          <div class="box-body">

            <!--Input name -->

            <div class="form-group">

              <div class="input-group">

                <span class="input-group-addon"><i class="fa fa-th"></i></span>

                <input class="form-control input-lg" type="text" name="editCategory" id="editCategory" required>
                <input class="form-control input-lg" type="hidden" name="idCategory" id="idCategory">

              </div>

            </div>

          </div>

        </div>

        <!--=====================================
        FOOTER
        ======================================-->

        <div class="modal-footer">

          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>

          <button type="submit"  class="btn btn-primary">Edit Category</button>

        </div>
        <?php 
      $editCategory= new CategoryController();
      $editCategory-> ctrEditCategory();
         ?>

      </form>

    </div>

  </div>

</div>