  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Quanta Dashboard Manage create sale
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
        <li class="active">Management create sale</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

<div class="row">
  <div class="col-lg-5 col-xs-12">
     <div class="box box-success">
       <div class="box-header with-border"></div>
         <form role="form" method="POST">
       <div class="box body">
           <div class="box">
            <!-- comment   -->
             <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-user"></i></span>
                  <input type="text" class="form-control" id="newVender" name="newVender" value="user Administrator" readonly>
                </div>        
             </div>
                         <!-- comment   -->
             <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-key"></i></span>
                  <input type="text" class="form-control" id="newSeller" name="newSeller" value="10098765" readonly>
                </div>        
             </div>
                                      <!-- comment   -->
             <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-users"></i></span>
                  <select class="form-control" name="selectClient" id="selectClient" required>
                    <option value="">Select Client</option>
                  </select>
                  <span class="input-group-addon"> <button type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#modalAddCustomer" data-dismiss="modal">Add Client</button></span>
                </div>        
             </div>
               <!-- Add product -->
               <div class="form-group row newProduct">
                 <div class="col-xs-5">
                   <div class="input-group">
                     <span class="input-group-addon"><button class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button></span>
                     <input type="text" class="form-control" id="addProduct" name="addProduct" placeholder="Product Description" required>
                   </div>
                 </div>
                   <div class="col-xs-3" style="padding-left:0px;">
                  
                   <input type="number" class="form-control" id="newProductQuantity" name="newProductQuantity" placeholder="0" required readonly>
                 </div>
                 <div class="col-xs-4" style="padding-left:0px;">
                  <div class="input-group">
                   <span class="input-group-addon">Ksh</span>
                   <input type="number" class="form-control" id="newProductPrice" name="newProductPrice" placeholder="00000" required readonly>
                 </div>
                 </div>
               </div>
               <button type="button" class="btn btn-success hidden-lg"> Add Product</button>
               <hr>
               <div class="row">
                <!-- input for total and taxes -->
                 <div class="col-xs-8 pull-right">
                   <table class="table">
                     <thead>
                       <tr>
                         <th>Taxies</th>
                         <th>Total</th>
                       </tr>
                     </thead>
                     <tbody>
                       <tr>
                         <td style="width:50%">
                  <div class="input-group">
                  <input type="number" class="form-control" id="newSellerTax" name="newSellerTax" placeholder="0">
                   <span class="input-group-addon"><span class="fa fa-percent"></span></span>
                 </div>
                         </td>
                           <td style="width:50%">
                  <div class="input-group">                    
                   <span class="input-group-addon"><span>Kshs</span></span>
                  <input type="number" class="form-control" id="newSellerTax" name="newSellerTax" placeholder="00000" readonly>
                 </div>
                         </td>
                       </tr>
                     </tbody>
                   </table>
                 </div>
               </div>
               <hr>
   <!-- PAMENT METHOD-->
   <div class="form-group  row"> 
   <div class="col-xs-6" style="padding-right: 0px;"> 
               <div class="input-group">
                             
                          <select class="form-control" name="newPaymentMethod" id="newPaymentMethod" required>
                            
                              <option value="">Select payment method</option>
                              <option value="cash">Cash</option>
                              <option value="CC">Credit Card</option>
                              <option value="DC">Debit Card</option>

                          </select>
                        </div>
                      </div>
                         <div class="col-xs-6 paymentMethodBoxes" style="padding-left: 0px">
                           <div class="input-group">
                  <input type="number" class="form-control" id="TransactionCode" name="TransactionCode" placeholder="Transaction code" readonly>
                   <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                 </div>
                         </div>
               </div>
           </div>
       
       </div>
      <div class="box-footer">
              <button type="submit" class="btn btn-primary pull-right">Save sale</button>
         </div>
     </form>
     </div>
  </div>
  <!-- add product table -->
<div class="col-lg-7 hidden-md hidden-sm hidden-xs">
 <div class="box box-warning">
   <div class="box-header with-border"></div>
   <div class="box-body">
      <table class="table table-bordered table-striped dt-responsive salesTable">
                  
                <thead>

                   <tr>
                     
                     <th style="width:10px">#</th>
                     <th>Image</th>
                     <th style="width:10px">Code</th>
                     <th>Description</th>
                     <th>Stock</th>
                     <th>Actions</th>

                   </tr> 

                </thead>
                <tbody>
                  <tr>
                    <td>1</td>
                    <td><img src="view/img/products/default/anonymous.png" alt="" width="40px"></td>
                    <td>508y098</td> 
                    <td>nnfldjlifjldkjnlmnfnflkjfkldnnlkmflfjjlkn</td>
                    <td>48</td>
                    <td><div class="btn-group">
  <button class="btn btn-info">Add</button>
  </div>
</td>
                  </tr>
                </tbody>

     </table>
   </div>
 </div> 
</div>
</div>
    </section>
  </div>


<!-- THE MODAL  CLIENT -->

<!-- Modal -->
<div id="modalAddCustomer" class="modal fade" role="dialog">

  <div class="modal-dialog">

   

    <div class="modal-content">

      <form role="form" method="POST" enctype="multipart/formdata">

        <!--=====================================
        HEADER
        ======================================-->

        <div class="modal-header" style="background: #3c8dbc; color: #fff">

          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title">Add Customer</h4>

        </div>

        <!--=====================================
        BODY
        ======================================-->

        <div class="modal-body">

          <div class="box-body">

            <!--Input name -->

            <div class="form-group">

              <div class="input-group">

                <span class="input-group-addon"><i class="fa fa-user"></i></span>

                <input class="form-control input-lg" type="text" name="newName" placeholder="Add name" required>

              </div>

            </div>
            <!-- input password -->

            <div class="form-group">

              <div class="input-group">

                <span class="input-group-addon"><i class="fa fa-key"></i></span>

                <input class="form-control input-lg" type="number" min="0" name="newDocumentId" placeholder="Id Number" required>

              </div>

            </div>
          <!-- input password -->

            <div class="form-group">

              <div class="input-group">

                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>

                <input class="form-control input-lg" type="email" name="newEmail" placeholder="Add Email" required>

              </div>

            </div>
    <!-- input password -->

            <div class="form-group">

              <div class="input-group">

                <span class="input-group-addon"><i class="fa fa-phone"></i></span>

                <input class="form-control input-lg" type="text" name="newPhone" placeholder="Add Phone" data-inputmask="'mask':'(999) 999-999999'" data-mask required>

              </div>

            </div>
       <!-- input password -->

            <div class="form-group">

              <div class="input-group">

                <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>

                <input class="form-control input-lg" type="text" name="newAddress" placeholder="Add Address" required>

              </div>

            </div>
           <!-- input password -->

            <div class="form-group">

              <div class="input-group">

                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>

                <input class="form-control input-lg" type="text" name="newDategoin" placeholder="Add Date" data-inputmask="'alias':'yyyy/mm/dd'" data-mask required>

              </div>

            </div>

            </div>

          </div>


        <!--=====================================
        FOOTER
        ======================================-->

        <div class="modal-footer">

          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>

          <button type="submit" class="btn btn-primary">Save customer</button>

        </div>

        <?php 
$createClient= new ControllerClients();
$createClient -> ctrCreateClient();

         ?>

      </form> 

    </div>

  </div>

</div>