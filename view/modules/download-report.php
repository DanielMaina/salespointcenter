<?php

require_once "../../controllers/sales.controller.php";
require_once "../../models/sales.model.php";
require_once "../../controllers/clients.controller.php";
require_once "../../models/clients.model.php";
require_once "../../controllers/users.controller.php";
require_once "../../models/users.model.php";

require_once "../../controllers/products.controller.php";
require_once "../../models/products.model.php";

$report = new SalesController();
$report -> ctrDownloadReport();