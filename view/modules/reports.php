<?php 
if ($_SESSION["profile"] == "seller" || $_SESSION["profile"]=="special" ) {
echo '
  <script>
window.location = "home";
  </script>  
 ';
 return;
}
   ?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Quanta Dashboard Manage Report
      </h1>
      <ol class="breadcrumb">
        <li><a href="home"><i class="fa fa-dashboard"></i>Home</a></li>
        <li class="active">Management Report </li>
      </ol>
    </section>

    <!-- Main content-->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <div class="input-group">
          <button type="button" class="btn btn-info" id="daterange-btn2">
          <span>
            <i class="fa fa-calendar"></i> Date Range 
          </span>
         <i class="fa fa-caret-down"></i> 
        </button> 
        </div>
          <div class="box-tools pull-right">
            <?php 

         if (isset($_GET["initialDate"])) {
            echo '<a href="view/modules/download-report.php?reports=reports&initialDate='.$_GET["initialDate"].'&finalDate='.$_GET["finalDate"].'">';
            }else{
              echo '<a href="view/modules/download-report.php?reports=reports">';
            }

             ?>
           
       
           <button class="btn btn-success" style="margin-top:5px;">Download Report</button></a>
          </div>
        </div>
        <div class="box-body">
     <div class="row">
       <div class="col-xs-12">
         <?php 

         include "reports/sales-graph.php";
         ?>
       </div>
       <div class="col-md-6 col-xs-12">
          <?php 

         include "reports/bestseller-products.php";
         ?>
       </div>
        <div class="col-md-6 col-xs-12">
          <?php 

         include "reports/sellers.php";
         ?>
       </div>
       <div class="col-md-6 col-xs-12">
          <?php 

         include "reports/buyers.php";
         ?>
       </div>
     </div>
        </div>
      
      </div>

    </section>
  </div>