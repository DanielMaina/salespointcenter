  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
      Page NOT Found!
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

   
    <section class="content">
<div class="error-page">
	<h2 class="headline text-primary">404</h2>
	<div class="error-content">
	 <h3><i class="fa fa-warning text-primary"></i> Oops! Page not found.</h3>
  <p> We could not find the page you were looking for. Meanwhile, you may <a href="home">return to Home</a> or try using the search form.
          </p>
	</div>
</div>

    </section>
  </div>
  <!-- /.content-wrapper -->