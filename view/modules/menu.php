<aside class="main-sidebar">
<section class="sidebar">
<ul class="sidebar-menu">
<?php 

if ($_SESSION["profile"] == "administrator") {
	

echo '<li class="active">
<a href="home"><i class="fa fa-home"></i><span>Home</span></a>	
</li>
<li>
<a href="users"><i class="fa fa-user"></i><span>Users</span></a>	
</li>';
}
if ($_SESSION["profile"]=="administrator" || $_SESSION["profile"]=="special")  {
echo '<li >
<a href="categories"><i class="fa fa-th"></i><span>Categories</span></a>	
</li>
<li>
<a href="products"><i class="fa fa-product-hunt"></i><span>Products</span></a>
</li>';
}	
if ($_SESSION["profile"] == "seller" || $_SESSION["profile"]=="administrator")  {
echo '<li>
<a href="clients"><i class="fa fa-users"></i><span>Client</span></a>	
</li>';
}
if ($_SESSION["profile"] == "seller" || $_SESSION["profile"]=="administrator")  {

echo '<li class="treeview">
<a href="#">
<i class="fa fa-list-ul"></i><span>Sales</span><span class="pull-right-container">
<i class="fa fa-angle-left pull-right"></i>
</span>	
</a>
<ul class="treeview-menu">
<li><a href="sales"><i class="fa fa-circle-o"></i><span>manage sales</span></a></li>
<li><a href="create-sales"><i class="fa fa-circle-o"></i><span>Create sales</span></a></li>';
if (($_SESSION["profile"] == "administrator" ) ) {

echo '<li><a href="reports"><i class="fa fa-circle-o"></i><span>manage Report</span></a></li> <li><a href="account"><i class="fa fa-circle-o"></i><span>manage Acc.</span></a></li>';
}

echo '</ul>
</li>';
}
?>
</ul>	
</section>
</aside>