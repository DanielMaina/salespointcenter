<?php 
if ($_SESSION["profile"]=="special" ) {
echo '
  <script>
window.location = "home";
  </script>  
 ';
 return;
}
?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Quanta Dashboard create sale
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
        <li class="active">Management create sale</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

<div class="row">
  <div class="col-lg-5 col-xs-12">
     <div class="box box-success">
       <div class="box-header with-border"></div>
         <form role="form" method="POST" class="formSales">
       <div class="box body">
           <div class="box">
            <!-- comment   -->
             <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-user"></i></span>
                  <input type="text" class="form-control" id="newSeller" name="newSeller" value="<?php echo $_SESSION['name'];?>" readonly>
                  <input type="hidden" name="idSeller" value="<?php echo $_SESSION['id'];?>">
                </div>        
             </div>
               <!-- comment   -->
             <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-key"></i></span>
                   <input type="text" class="form-control" id="newSales" name="newSales" required placeholder="Enter R/I No."/>
                </div>        
             </div>
                                      <!-- comment   -->
             <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon"><i class="fa fa-users"></i></span>
                  <select class="form-control" name="selectClient" id="selectClient" required>
                    <option value="">Select Client</option>
                      <?php 
                $item = null;
                $value = null;
                $categories=ControllerClients::ctrShowClients($item,$value);
                foreach ($categories as $key => $value) {
                  echo '<option value="'.$value["id"].'">'.$value["name"].'</option>';
                }
                   ?>
                  </select>
                  <span class="input-group-addon"> <button type="button" class="btn btn-info btn-xs" data-toggle="modal" data-target="#modalAddCustomer" data-dismiss="modal">Add Client</button></span>
                </div>        
             </div>
               <!-- Add product -->
               <div class="form-group row newProduct">
                
               </div>
               <button type="button" class="btn btn-success hidden btnAddProduct"> Add Product</button>
               <hr>
               <div class="row">
                <!-- input for total and taxes -->
                 <div class="col-xs-12 pull-right">
                   <table class="table">
                     <thead>
                       <tr>
                         <th>Charges</th>
                         <th>Total</th>
                       </tr>
                     </thead>
                     <tbody>
                       <tr>
                         <td style="width:50%">
                  <div class="input-group">
                  <input type="number" class="form-control" id="newSellerTax" name="newSellerTax" placeholder="0" readonly>
                  <input type="hidden" id="newPriceTax" name="newPriceTax">
                  <input type="hidden" id="newNetPrice" name="newNetPrice">
                   <span class="input-group-addon"><span class="fa fa-percent"></span></span>
                 </div>
                         </td>
                           <td style="width:50%">
                  <div class="input-group">                    
                   <span class="input-group-addon"><span>Kshs</span></span>
                  <input type="text" class="form-control" id="newSellerPrice" total="" name="newSellerPrice" placeholder="00000" readonly>
                  <input type="hidden"  name="totalSale" id="totalSale">
                 </div>
                         </td>
                       </tr>
                     </tbody>
                   </table>
                 </div>
               </div>
               <hr>
   <!-- PAMENT METHOD-->
   <div class="form-group  row"> 
   <div class="col-xs-6" style="padding-right: 0px;"> 
               <div class="input-group">
                             
                          <select class="form-control" name="newPaymentMethod" id="newPaymentMethod" required>
                            
                              <option value="">Select payment method</option>
                              <option value="Cash">Cash</option>
                              <option value="MP">M-Pesa</option>
                              <option value="CQ">Cheque</option>                           
                              <option value="IN">Invoice</option>                           

                          </select>
                        </div>
                      </div>
                      <div class="paymentMethodBoxes"></div>
                      <input type="hidden" id="productsList" name="productsList">
                      <input type="hidden" name="listPaymentMethod" id="listPaymentMethod" >
               </div>
           </div>
       
       </div>
      <div class="box-footer">
              <button type="submit" class="btn btn-primary pull-right">Save sale</button>
         </div>
     </form>
      <?php 
$saveSales=new SalesController();
$saveSales -> ctrCreateSale();
       ?>
      
     </div>
  </div>
  <!-- add product table -->
<div class="col-lg-7 hidden-md hidden-sm hidden-xs">
 <div class="box box-warning">
   <div class="box-header with-border"></div>
   <div class="box-body">
      <table class="table table-bordered table-striped dt-responsive salesTable">
                  
                <thead>

                   <tr>
                     
                     <th style="width:10px">#</th>
                     <th>Image</th>
                     <th style="width:10px">Code</th>
                     <th>Description</th>
                     <th>Stock</th>
                     <th>Actions</th>

                   </tr> 

                </thead>
                <tbody>
  
                </tbody>

     </table>
   </div>
 </div> 
</div>
</div>
    </section>
  </div>


<!-- THE MODAL  CLIENT -->

<!-- Modal -->
<div id="modalAddCustomer" class="modal fade" role="dialog">

  <div class="modal-dialog">

   

    <div class="modal-content">

      <form role="form" method="POST" enctype="multipart/formdata">

        <!--=====================================
        HEADER
        ======================================-->

        <div class="modal-header" style="background: #3c8dbc; color: #fff">

          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title">Add Customer</h4>

        </div>

        <!--=====================================
        BODY
        ======================================-->

        <div class="modal-body">

          <div class="box-body">

            <!--Input name -->

            <div class="form-group">

              <div class="input-group">

                <span class="input-group-addon"><i class="fa fa-user"></i></span>

                <input class="form-control input-lg" type="text" name="newName" placeholder="Add name" required>

              </div>

            </div>
            <!-- input password -->

            <div class="form-group">

              <div class="input-group">

                <span class="input-group-addon"><i class="fa fa-key"></i></span>

                <input class="form-control input-lg" type="number" min="0" name="newDocumentId" placeholder="Id Number" required>

              </div>

            </div>
          <!-- input password -->

            <div class="form-group">

              <div class="input-group">

                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>

                <input class="form-control input-lg" type="email" name="newEmail" placeholder="Add Email" required>

              </div>

            </div>
    <!-- input password -->

            <div class="form-group">

              <div class="input-group">

                <span class="input-group-addon"><i class="fa fa-phone"></i></span>

                <input class="form-control input-lg" type="text" name="newPhone" placeholder="Add Phone" data-inputmask="'mask':'(999) 999-999999'" data-mask required>

              </div>

            </div>
       <!-- input password -->

            <div class="form-group">

              <div class="input-group">

                <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>

                <input class="form-control input-lg" type="text" name="newAddress" placeholder="Add Address" required>

              </div>

            </div>
           <!-- input password -->

            <div class="form-group">

              <div class="input-group">

                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>

                <input class="form-control input-lg" type="text" name="newDategoin" placeholder="Add Date" data-inputmask="'alias':'yyyy/mm/dd'" data-mask required>

              </div>

            </div>

            </div>

          </div>


        <!--=====================================
        FOOTER
        ======================================-->

        <div class="modal-footer">

          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>

          <button type="submit" class="btn btn-primary">Save customer</button>

        </div>

        <?php 
$createClient= new ControllerClients();
$createClient -> ctrCreateClient();

         ?>


      </form> 


    </div>

  </div>

</div>