<header class="main-header">
<!--=============================================
	=            DESIGN HEADER LOG           =
	=============================================-->
<a href="home" class="logo">
	<!-- small screen imagelogo -->
<span class="logo-mini">
	<img class="img-responsive" src="view/img/template/log-quanta.jpg" style="padding: 10px;">
</span>	
<!-- normal log large -->
<span class="logo-lg">
	<img class="img-responsive" src="view/img/template/logo-quanta.png" style="padding: 0px 0;">
</span>	
</a>		

<!--=============================================
	=           NavigationBAR         =
	=============================================-->
<nav class="navbar navbar-static-top">
	<!-- naigation btn -->
 <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
<span class="sr-only">Toggle navigation</span>

</a>
<!-- logedin profile -->
<div class="navbar-custom-menu">
<ul class="nav navbar-nav">
<li class="dropdown user user-menu">
<a href="#" class="dropdown-toggle" data-toggle="dropdown">
<?php 
if ($_SESSION["photo"] != "") {
echo '<img src="'.$_SESSION["photo"].'" class="user-image">	';	
}else{
	echo '<img src="view/img/users/default/anonymous.png" class="user-image">';
}
 ?>		

<span class="hidden-xs"><?php echo $_SESSION['name']; ?></span>
</a>
<!-- dropdown toggle -->
<ul class="dropdown-menu">
<li class="user-body">
<div class="pull-right">
<a href="logout" class="btn btrn-default btn-flat">Logout</a>	
</div>	
	
</li>	
</ul>
</li>	
</ul>	
</div>

</nav>
</header>