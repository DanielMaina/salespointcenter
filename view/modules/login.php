<div id="back"></div>
<div class="login-box">
  
  <div class="login-logo">
        <img src="view/img/template/logo-quanta.png" alt="" class="img-responsive"
             style="padding: 10px 0px 0px 0px" >
    </div>

  <div class="login-box-body">
    <p class="login-box-msg">Login To Quanta System</p>

    <form method="post">
      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Username" name="username" required>
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Password" name="password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">LogIn</button>
        </div>
       
      </div>
      <?php 
$login= new UserController();
$login->ctrLoginUser();

       ?>
    </form>

  </div>
</div>