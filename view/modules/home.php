
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Quanta Dashboard
        <small>Control Panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <section class="content">
<div class="row">
  <?php 
  if ($_SESSION["profile"] == "administrator") {
   
   include "home/top-boxes.php";
 }
   ?>
</div>
<div class="row">
  <div class="col-lg-12">
  <?php 
   if ($_SESSION["profile"] == "administrator") {
   
   include "reports/sales-graph.php";
 }
   ?>
 </div>
 <div class="col-lg-6">
   <?php 
    if ($_SESSION["profile"] == "administrator") {
   
include "reports/bestseller-products.php";
}
    ?>
 </div>
 <div class="col-lg-6">
   <?php 
    if ($_SESSION["profile"] == "administrator") {
   
include "home/recent-products.php";
}
    ?>
 </div>
 <div class="col-lg-12">
   <?php 
 if ($_SESSION["profile"] == "seller" || $_SESSION["profile"]=="special") {
  echo '<div class="box box-info">
  <h1 class="">Welcome @ '.$_SESSION["name"].' Please navigate to your role</h1> 
  </div>';
   }
    ?>
 </div>
</div>
    </section>
  </div>