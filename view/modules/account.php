<?php 
if ($_SESSION["profile"]=="special" ) {
echo '
  <script>
window.location = "home";
  </script>  
 ';
 return;
}
   ?><!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Quanta Manage Account Dashboard
      </h1>
      <ol class="breadcrumb">
        <li><a href="home"><i class="fa fa-dashboard"></i>Account</a></li>
        <li class="active">Account Management</li>
      </ol>
    </section>

  
    <section class="content">
<div class="box">
<div class="box-header with-border">
<button type="button" class="btn btn-info pull-left" id="daterange-btn-acc">
  <span>
    <i class="fa fa-calendar"></i> Date Range 
  </span>
 <i class="fa fa-caret-down"></i> 
</button>
<div class="pull-right">
<div class="row">
<div class="col-lg-4">
<a class="btn btn-success btn-sm" href="view/modules/download-debtReport.php?debtor=debtor">
Download Debts</a>
</div>
</div>
</div>   
</div>
<div class="box-body">
<table class="table table-bordered table-striped dt-responsive tables">
 <thead>
 <tr>
 <th style="width: 10px;">#</th> 
  <th>Bill code</th> 
  <th>Customer</th> 
  <th>Pay Status</th> 
  <th>method</th> 
  <th>Total</th> 
  <th>Paid</th> 
  <th>Balance</th> 
  <th>Date</th> 
  <th>Actions</th> 
 </tr> 
 </thead> 
 <tbody>
  <?php 
  if (isset($_GET["initialDate"])){

    $initialDate=$_GET["initialDate"];
    $finalDate=$_GET["finalDate"];

  }else{

   $initialDate=null;
   $finalDate=null; 

  }
$response =SalesController::ctrSalesDatesRange($initialDate, $finalDate);

 foreach($response as $key => $value){
   echo ' <tr>
  <td>'.($key+1).'</td> 
  <td>'.$value["code"].'</td>';

  $itemClient = "id";
  $valueClient =$value["id_client"];
  $responseClient = ControllerClients::ctrShowClients($itemClient, $valueClient);
  echo'<td>'.$responseClient["name"].'</td>';
  echo '
  <td>';
  $receiptNo="receipt_no";
  $receiptValue=$value["code"];
  $result=SalesController::ctrSeeDebt($receiptNo,$receiptValue);
  $totalP=0;
  foreach($result as $key => $Invalue){ 
  $totalP +=intval($Invalue["amount_paid"]);
  }
  $status=$value["total"]-$totalP;
if($status===0){
  echo "<span class='btn btn-success btn-xs'>Completed</span>";
}else if($status==$value["total"]){
  echo "<span class='btn btn-danger btn-xs'>Not Paid!</span>";
}else if($status >0 && $status < intval($value["total"])){
  echo "<span class='btn btn-warning btn-xs'>Not Cleared!</span>";
}else{
  echo "<span class='btn btn-info btn-xs'>Sorry Error!</span>";
}

  echo'</td> 
  <td>'.$value["payment_method"].'</td>
  <td>' .number_format($value["total"],2).'</td>
  <td>'.$totalP.'</td>
  <td>';
  echo($status);

  echo'</td>
  <td>'.substr($value["date"],0,-8).'</td> 
  <td>
  <div class="btn-group">
  <button class="btn btn-info btnPrintDebtPayment" saleCode="'.$value["code"].'" title="Track payment"><i class="fa fa-eye"></i></button>';
 
 if ($_SESSION["profile"]=="administrator"){
 if($status===0){
  echo '<button class="btn btn-warning "  title="Sorry Already Completed"><i class="fa fa-pencil"></i></button>';
 }else{
  echo '<button data-toggle="modal" data-target="#modalPayDebt" class="btn btn-warning btnPayDebt" receiptNo="'.$value["code"].'" title="update Debt payment"><i class="fa fa-pencil"></i></button>';

}
 }
  echo '</div>
  </td> 
 </tr> ';
 }

    ?>
 </tbody>
</table>
<h1>handle Expense</h1><hr>

<div class="box-header with-border">
<button class="btn btn-primary" data-toggle="modal" data-target="#addExpenses">Add Expenses</button>    
</div>

<table class="table table-bordered table-striped td-responsive tables">
<thead>
<tr>
<th>#</th>
<th>Amount</th>
<th>Remark</th>
<th>Date</th>
</tr>
</thead>
<tbody>
<?php
$item=null;
$value=null;
$expResult=SalesController::ctrSeeExpenses($item,$value);
foreach($expResult as $key => $value){
echo '
<tr>
<td>'.($key+1).'</td>
<td>'.$value["amount"].'</td>
<td>'.$value["remark"].'</td>
<td>'.$value["pay_date"].'</td>
</tr>
';
}
?>
</tbody>
</table>

<h1>Get Profile</h1>       
</div>
       
 </div>

</section>
 
</div>

<!-- MODAL TO PAY DEBT -->
<div id="modalPayDebt" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<form role="form" method="POST">
        <!--=====================================
        HEADER
        ======================================-->

        <div class="modal-header" style="background: #3c8dbc; color: #fff">

          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title">Please write <strong>Amount</strong> Paid Today</h4>
        </div>
        <!--=====================================
        BODY
        ======================================-->

        <div class="modal-body">

          <div class="box-body">

            <!--Input Amount -->

            <div class="form-group">

              <div class="input-group">

                <span class="input-group-addon"><i class="fa fa-money"></i></span>

                <input class="form-control input-lg" type="number" name="moneyPaid" id="moneyPaid" placeholder="Enter Amount (Kshs)" required>

                <input type="hidden" id="receiptNo" name="ReceiptNo">
              </div>

            </div>
            <!-- input payment -->

            <div class="form-group">

              <div class="input-group">

                <span class="input-group-addon"><i class="fa fa-key"></i></span>

                <select class="form-control input-lg"  name="payType" id="payType" required>
                <option value="">Select Payment Method</option>
                <option value="Cash">Cash</option>
                <option value="M-Pesa">M-pesa</option>
                <option value="Invoice">Invoice</option>
                </select>

              </div>

            </div>
          <!-- input remark -->

            <div class="form-group">

              <div class="input-group">

                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>

                <textarea class="form-control input-lg" name="remark" id="remark" placeholder="Remark..." required></textarea>

              </div>

            </div>
            </div>

          </div> 
                  <!--=====================================
        FOOTER
        ======================================-->

        <div class="modal-footer">

<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>

<button type="submit" name="submitDebt" class="btn btn-primary">Update Payment</button>

<?
$updateDebt= new SalesController();
$updateDebt->ctrUpdateDebt();
?>
</div>
</form>
</div>
</div>
</div>

<!-- MODAL TO HANDLE EXPENSES -->

<div id="addExpenses" class="modal fade" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<form role="form" method="POST">
        <!--=====================================
        HEADER
        ======================================-->

        <div class="modal-header" style="background: #3c8dbc; color: #fff">

          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title"><strong>Write Each Expense</strong></h4>
        </div>
        <!--=====================================
        BODY
        ======================================-->

        <div class="modal-body">

          <div class="box-body">

            <!--Input Amount -->

            <div class="form-group">

              <div class="input-group">

                <span class="input-group-addon"><i class="fa fa-money"></i></span>

                <input class="form-control input-lg" type="number" name="expAmount" id="expAmount" placeholder="Enter Amount (Kshs)" required>
              </div>

            </div>
          <!-- input remark -->

            <div class="form-group">

              <div class="input-group">

                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>

                <textarea class="form-control input-lg" name="expRemark" id="expRemark" placeholder="Describe..." required></textarea>

              </div>

            </div>
            </div>

          </div> 
                  <!--=====================================
        FOOTER
        ======================================-->

        <div class="modal-footer">

<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>

<button type="submit" name="submitExpense" class="btn btn-primary">Save</button>

<?php 
$payExpense= new SalesController();
$payExpense->ctrPayExpenses();
?>
</div>
</form>
</div>
</div>
</div>

