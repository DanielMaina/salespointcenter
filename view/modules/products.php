<?php 
if ($_SESSION["profile"] == "seller") {
echo '
  <script>
window.location = "home";
  </script>  
 ';
 return;
}
   ?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Quanta Manage Product Dashboard
      </h1>
      <ol class="breadcrumb">
        <li><a href="home"><i class="fa fa-dashboard"></i>Home</a></li>
        <li class="active">Product Management</li>
      </ol>
    </section>

  
    <section class="content">

      
      <div class="box">
        <div class="box-header with-border">
<button class="btn btn-primary" data-toggle="modal" data-target="#addProduct">Add Product</button>

<div class="box-tools pull-right">
<?php echo '<a href="view/modules/download-product.php?products=products">'; ?>
<button class="btn btn-success" style="margin-top:5px;">Download Product</button></a>
</div>    
</div>
<div class="box-body">
<table class="table table-bordered table-striped dt-responsive  tableProducts">
 <thead>
 <tr>
 <th style="width: 10px;">#</th> 
  <th>Images</th> 
  <th>Code</th> 
  <th>Description</th> 
  <th>Category</th> 
  <th>Stock</th> 
  <th>Buying Price</th> 
  <th>Sale Price</th> 
  <th>Add Date</th>
  <th>Actions</th>
 </tr> 
 </thead> 

</table> 
<input type="hidden" value="<?php echo $_SESSION["profile"] ?>" id="hiddenProfile">       
</div>
       
 </div>
</section>
 
</div>
 
<!-- MODAL ADD PRODUCT!!! -->

<!-- Modal -->
<div id="addProduct" class="modal fade" role="dialog">

  <div class="modal-dialog">

   

    <div class="modal-content">

      <form role="form" method="POST" enctype="multipart/form-data" >
        <!--=====================================
        HEADER
        ======================================-->

        <div class="modal-header" style="background: #3c8dbc; color: #fff">

          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title">Add product</h4>

        </div>

        <!--=====================================
        BODY
        ======================================-->

        <div class="modal-body">

          <div class="box-body">
            <!-- input select -->
            <div class="form-group">

              <div class="input-group">

                <span class="input-group-addon"><i class="fa fa-th"></i></span>

                <select class="form-control input-lg" id="newCategory" name="newCategory" required>
                 
                  <option value="">Select Category</option>
                  <?php
                  $item=null;
                  $value=null;
                  $categories= CategoryController::crtShowCategory($item,$value);
                    foreach($categories as $key => $value) {
                      echo '<option value="'.$value["id"].'">'.$value["category"].'</option>';
                    }
                   ?>
                </select>

              </div>

            </div>

            <!--Input -->

            <div class="form-group">

              <div class="input-group">

                <span class="input-group-addon"><i class="fa fa-code"></i></span>

                <input class="form-control input-lg" type="text" name="newCode" id="newCode" placeholder="new Code" readonly>

              </div>

            </div>

            <!-- input -->

            <div class="form-group">

              <div class="input-group">

                <span class="input-group-addon"><i class="fa fa-product-hunt"></i></span>

                <input class="form-control input-lg" type="text" name="newDescription" placeholder="Add Description" required>

              </div>

            </div>
            <!-- input  -->
            <div class="form-group">

              <div class="input-group">

                <span class="input-group-addon"><i class="fa fa-check"></i></span>

                <input class="form-control input-lg" type="number" name="newStock" placeholder="Add Stock" min="0" required>

              </div>

            </div>

            <div class="form-group row">
              <div class="col-xs-12 col-sm-6">
              <div class="input-group">

                <span class="input-group-addon"><i class="fa fa-arrow-up"></i></span>

                <input class="form-control input-lg" type="number" name="newBuyingPrice" id="newBuyingPrice" step="any" min="0" placeholder="Add Buying Price" required>

              </div>
              </div>
              <div class="col-xs-12 col-sm-6">
              <div class="input-group">

                <span class="input-group-addon"><i class="fa fa-arrow-down"></i></span>

                <input class="form-control input-lg" type="number" name="newSellingPrice" id="newSellingPrice" min="0" step="any" placeholder="Add Selling Price" required>

              </div>
              <br>
              <div class="col-xs-4" style="padding:0;">
               <div class="form-group">
                 <label for="">
                   <input type="checkbox" class="minimal percentage" checked>
                   Use %
                 </label>
               </div> 
              </div>
              <div class="col-xs-8">
                <div class="input-group">
                  <input type="number" class="form-control input-lg newPercentage" min="0" value="10" required>
                  <span class="input-group-addon"> <i class="fa fa-percent"></i></span>
                </div>
              </div>
            </div>
            </div>

            <div class="form-group">

              <div class="panel">Upload image</div>

              <input class="newImage" type="file" name="newImage">

              <p class="help-block">Maximum size 2Mb</p>

              <img src="view/img/products/default/anonymous.png" class="img-thumbnail previsualizer" width="100px">

            </div>

          </div>

        </div>

        <!--=====================================
        FOOTER
        ======================================-->

        <div class="modal-footer">

          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>

          <button type="submit" class="btn btn-primary">Save Product</button>

        </div>

      <?php 
       $createProduct = new ProductsController();
       $createProduct -> ctrCreateProduct(); 
       ?>
      </form>
    </div>

  </div>

</div>



<!-- MODAL EDIT PRODUCT!!! -->

<!-- Modal -->
<div id="modalEditProduct" class="modal fade" role="dialog">

  <div class="modal-dialog">

   

    <div class="modal-content">

      <form role="form" method="POST" enctype="multipart/form-data" >
        <!--=====================================
        HEADER
        ======================================-->

        <div class="modal-header" style="background: #3c8dbc; color: #fff">

          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title">Edit product</h4>

        </div>

        <!--=====================================
        BODY
        ======================================-->

        <div class="modal-body">

          <div class="box-body">
            <!-- input select -->
            <div class="form-group">

              <div class="input-group">

                <span class="input-group-addon"><i class="fa fa-th"></i></span>

                <select class="form-control input-lg"  name="editCategory" readonly>
                 
                  <option id="editCategory"></option>
                </select>

              </div>

            </div>

            <!--Input -->

            <div class="form-group">

              <div class="input-group">

                <span class="input-group-addon"><i class="fa fa-code"></i></span>

                <input class="form-control input-lg" type="text" name="editCode" id="editCode" placeholder=" Code" readonly>
                <input type="hidden" name="editId" id="editId">
                <input type="hidden" name="editSales" id="editSales">

              </div>

            </div>

            <!-- input -->

            <div class="form-group">

              <div class="input-group">

                <span class="input-group-addon"><i class="fa fa-product-hunt"></i></span>

                <input class="form-control input-lg" type="text" name="editDescription" id="editDescription"  required>

              </div>

            </div>
            <!-- input  -->
            <div class="form-group">

              <div class="input-group">

                <span class="input-group-addon"><i class="fa fa-check"></i></span>

                <input class="form-control input-lg" type="number" name="editStock" id="editStock" min="0" required>

              </div>

            </div>

            <div class="form-group row">
              <div class="col-xs-12 col-sm-6">
              <div class="input-group">

                <span class="input-group-addon"><i class="fa fa-arrow-up"></i></span>

                <input class="form-control input-lg" type="number" name="editBuyingPrice" id="editBuyingPrice" step="any" min="0"  required>

              </div>
              </div>
              <div class="col-xs-12 col-sm-6">
              <div class="input-group">

                <span class="input-group-addon"><i class="fa fa-arrow-down"></i></span>

                <input class="form-control input-lg" type="number" name="editSellingPrice" id="editSellingPrice" min="0" step="any" readonly>

              </div>
              <br>
              <div class="col-xs-4" style="padding:0;">
               <div class="form-group">
                 <label for="">
                   <input type="checkbox" class="minimal percentage" checked>
                   Use %
                 </label>
               </div> 
              </div>
              <div class="col-xs-8">
                <div class="input-group">
                  <input type="number" class="form-control input-lg newPercentage" min="0" value="10" required>
                  <span class="input-group-addon"> <i class="fa fa-percent"></i></span>
                </div>
              </div>
            </div>
            </div>

            <div class="form-group">

              <div class="panel">Upload image</div>

              <input class="newImage" type="file" name="editImage">

              <p class="help-block">Maximum size 2Mb</p>

              <img src="view/img/products/default/anonymous.png" class="img-thumbnail previsualizer" width="100px">
             <input type="hidden" name="actualImage" id="actualImage">
            </div>

          </div>

        </div>

        <!--=====================================
        FOOTER
        ======================================-->

        <div class="modal-footer">

          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>

          <button type="submit" name="submitEditProduct" class="btn btn-primary">Update Product</button>

        </div>
     <?php 
       $editProduct = new ProductsController();
       $editProduct -> ctrEditProduct(); 
       ?>
      </form>
    </div>

  </div>

</div>
 <?php 
  $deleteProduct = new ProductsController();
  $deleteProduct -> ctrDeleteProduct(); 
 ?>
