<?php

$item = null;
$value = null;
$order = "id";

$sales = SalesController::ctrAddingTotalSales();

$categories = CategoryController::crtShowCategory($item, $value);
$totalCategories = count($categories);

$customers = ControllerClients::ctrShowClients($item, $value);
$totalCustomers = count($customers);

$products = ProductsController::ctrShowProduct($item, $value, $order);
$totalProducts = count($products);
//For stockInCash
$ftotalStock=0;
foreach ($products as $key => $value) {
  $totalStock=$value["stock"]*$value["buying_price"];
 $ftotalStock +=$totalStock;
};
?>
<!-- TETST THE THING OUT -->

        
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><a href="sales" style="color: white;"><i class="ion ion-social-usd"></i></a></span>

            <div class="info-box-content">
              <span class="info-box-text">Sales</span>
              <span class="info-box-number">Kshs <?php echo number_format($sales["total"],2); ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><a href="clients" style="color: white;"><i class="ion ion-person-add"></i></a></span>

            <div class="info-box-content">
              <span class="info-box-text">Customer</span>
              <span class="info-box-number"><?php echo number_format($totalCustomers); ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><a href="categories" style="color: white;"><i class="ion ion-clipboard"></i></a></span>

            <div class="info-box-content">
              <span class="info-box-text">Category</span>
              <span class="info-box-number"><?php echo number_format($totalCategories); ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><a href="products" style="color: white;"><i class="ion ion-ios-cart"></i></a></span>

            <div class="info-box-content">
              <span class="info-box-text">Products(<?php echo number_format($totalProducts); ?>)</span>
              <span class="info-box-number">Kshs <?php echo number_format($ftotalStock,2); ?></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
