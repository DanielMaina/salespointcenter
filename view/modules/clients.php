<?php 
if ($_SESSION["profile"] == "special") {
echo '
  <script>
window.location = "home";
  </script>  
 ';
 return;
}
   ?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Quanta Manage Client Dashboard
      </h1>
      <ol class="breadcrumb">
        <li><a href="home"><i class="fa fa-dashboard"></i>Customer</a></li>
        <li class="active">Customers Management</li>
      </ol>
    </section>

  
    <section class="content">

      
      <div class="box">
        <div class="box-header with-border">
<button class="btn btn-primary" data-toggle="modal" data-target="#addCustomer">Add Customer</button>
         
</div>
<div class="box-body">
<table class="table table-bordered table-striped dt-responsive tables ">
 <thead>
 <tr>
 <th style="width: 10px;">#</th> 
  <th>Name</th> 
  <th>ID No.</th> 
  <th>Email</th> 
  <th>phone</th> 
  <th>address</th> 
  <th>member since</th> 
  <th>Total purchase</th>
  <th>Last purchase</th>
  <th>Last login</th> 
  <th>Actions</th> 
 </tr> 
 </thead> 
 <tbody>
  <?php 
$item=null;
$value=null;
$client=ControllerClients::ctrShowClients($item,$value);
foreach ($client as $key => $value) {
  $id=$value["id"];
$name=$value["name"];
$document=$value["document"];
$email=$value["email"];
$phone=$value["phone"];
$address=$value["address"];
$join_date=$value["join_date"];
$purchase=$value["purchase"];
$last_purchases=$value["last_purchases"];
$lastlogin=$value["lastlogin"];

   ?>
 <tr>
  <td><?php echo($key +1) ?></td> 
  <td><?php echo($name); ?></td> 
  <td><?php echo($document); ?></td> 
  <td><?php echo($email);?></td> 
  <td><?php echo($phone); ?></td>
  <td><?php echo($address); ?></td>
  <td><?php echo($join_date); ?></td>
  <td><?php 
if($purchase == null) {
  echo "<p class='text-danger'>N/A</p>";
}else{
  echo($purchase); 
}?></td> 
  <td><?php echo($last_purchases); ?></td> 
  <td><?php echo($lastlogin); ?></td> 
  <td>
    <div class="btn-group">
      <button class="btn btn-warning btnEditClient" data-toggle="modal" data-target="#modalEditClient" idClient=<?php echo $id; ?> ><i class="fa fa-pencil"></i></button>
      <?php if ($_SESSION["profile"]== "administrator") {
        ?>
      <button class="btn btn-danger btnDeleteClient" idClient=<?php echo $id; ?> ><i class="fa fa-times"></i></button>
    <?php } ?>
    </div>
  </td> 
 </tr> 
<?php } ?>
 </tbody>
</table>
<?php 
$deleteclient= new ControllerClients();
$deleteclient -> ctrDeleteClient();
 ?>        
</div>
       
 </div>

</section>
 
</div>
 
<!-- THE MODAL  CLIENT -->

<!-- Modal -->
<div id="addCustomer" class="modal fade" role="dialog">

  <div class="modal-dialog">

   

    <div class="modal-content">

      <form role="form" method="POST" enctype="multipart/formdata">

        <!--=====================================
        HEADER
        ======================================-->

        <div class="modal-header" style="background: #3c8dbc; color: #fff">

          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title">Add Customer</h4>

        </div>

        <!--=====================================
        BODY
        ======================================-->

        <div class="modal-body">

          <div class="box-body">

            <!--Input name -->

            <div class="form-group">

              <div class="input-group">

                <span class="input-group-addon"><i class="fa fa-user"></i></span>

                <input class="form-control input-lg" type="text" name="newName" placeholder="Add name" required>

              </div>

            </div>
            <!-- input password -->

            <div class="form-group">

              <div class="input-group">

                <span class="input-group-addon"><i class="fa fa-key"></i></span>

                <input class="form-control input-lg" type="number" min="0" name="newDocumentId" placeholder="Id Number" required>

              </div>

            </div>
          <!-- input password -->

            <div class="form-group">

              <div class="input-group">

                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>

                <input class="form-control input-lg" type="email" name="newEmail" placeholder="Add Email" required>

              </div>

            </div>
    <!-- input password -->

            <div class="form-group">

              <div class="input-group">

                <span class="input-group-addon"><i class="fa fa-phone"></i></span>

                <input class="form-control input-lg" type="text" name="newPhone" placeholder="Add Phone" data-inputmask="'mask':'(999) 999-999999'" data-mask required>

              </div>

            </div>
       <!-- input password -->

            <div class="form-group">

              <div class="input-group">

                <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>

                <input class="form-control input-lg" type="text" name="newAddress" placeholder="Add Address" required>

              </div>

            </div>
           <!-- input password -->

            <div class="form-group">

              <div class="input-group">

                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>

                <input class="form-control input-lg" type="text" name="newDategoin" placeholder="Add Date" data-inputmask="'alias':'yyyy/mm/dd'" data-mask required>

              </div>

            </div>

            </div>

          </div>


        <!--=====================================
        FOOTER
        ======================================-->

        <div class="modal-footer">

          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>

          <button type="submit" class="btn btn-primary">Save customer</button>

        </div>
            <?php 
$createClient= new ControllerClients();
$createClient -> ctrCreateClient();

         ?>
   </form>      
    </div>

  </div>

</div>


 
<!-- THE MODAL EDIT  CLIENT -->

<!-- Modal -->
<div id="modalEditClient" class="modal fade" role="dialog">

  <div class="modal-dialog">

   

    <div class="modal-content">

      <form role="form" method="POST">

        <!--=====================================
        HEADER
        ======================================-->

        <div class="modal-header" style="background: #3c8dbc; color: #fff">

          <button type="button" class="close" data-dismiss="modal">&times;</button>

          <h4 class="modal-title">Edit Customer</h4>

        </div>

        <!--=====================================
        BODY
        ======================================-->

        <div class="modal-body">

          <div class="box-body">

            <!--Input name -->

            <div class="form-group">

              <div class="input-group">

                <span class="input-group-addon"><i class="fa fa-user"></i></span>

                <input class="form-control input-lg" type="text" name="editName" id="editName" required>
                <input type="hidden" id="idClient" name="idClient">

              </div>

            </div>
            <!-- input password -->

            <div class="form-group">

              <div class="input-group">

                <span class="input-group-addon"><i class="fa fa-key"></i></span>

                <input class="form-control input-lg" type="number" min="0" name="editDocumentId" id="editDocumentId" required>

              </div>

            </div>
          <!-- input password -->

            <div class="form-group">

              <div class="input-group">

                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>

                <input class="form-control input-lg" type="email" name="editEmail" id="editEmail" required>

              </div>

            </div>
    <!-- input password -->

            <div class="form-group">

              <div class="input-group">

                <span class="input-group-addon"><i class="fa fa-phone"></i></span>

                <input class="form-control input-lg" type="text" name="editPhone" id="editPhone" data-inputmask="'mask':'(999) 999-999999'" data-mask required>

              </div>

            </div>
       <!-- input password -->

            <div class="form-group">

              <div class="input-group">

                <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>

                <input class="form-control input-lg" type="text" name="editAddress" id="editAddress" required>

              </div>

            </div>
           <!-- input password -->

            <div class="form-group">

              <div class="input-group">

                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>

                <input class="form-control input-lg" type="text" name="editDategoin" id="editDategoin" data-inputmask="'alias':'yyyy/mm/dd'" data-mask required>

              </div>

            </div>

            </div>

          </div>


        <!--=====================================
        FOOTER
        ======================================-->

        <div class="modal-footer">

          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>

          <button type="submit" class="btn btn-primary">Edit Customer</button>

        </div>

  <?php 

$editclient= new ControllerClients();
$editclient -> ctrEditClient();
   ?>
      </form> 

    </div>

  </div>

</div>