<?php 
if ($_SESSION["profile"]=="special" ) {
echo '
  <script>
window.location = "home";
  </script>  
 ';
 return;
}
   ?><!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Quanta Manage sales Dashboard
      </h1>
      <ol class="breadcrumb">
        <li><a href="home"><i class="fa fa-dashboard"></i>Sales</a></li>
        <li class="active">Sales Management</li>
      </ol>
    </section>

  
    <section class="content">
<div class="box">
<div class="box-header with-border">
<a href="create-sales"><button class="btn btn-primary">Add Sales</button></a> 
<button type="button" class="btn btn-info pull-right" id="daterange-btn">
  <span>
    <i class="fa fa-calendar"></i> Date Range 
  </span>
 <i class="fa fa-caret-down"></i> 
</button>   
</div>
<div class="box-body">
<table class="table table-bordered table-striped dt-responsive tables ">
 <thead>
 <tr>
 <th style="width: 10px;">#</th> 
  <th>Bill code</th> 
  <th>Customer</th> 
  <th>Seller</th> 
  <th>Payment method</th> 
  <th>Net price</th> 
  <th>Total</th> 
  <th>Date</th> 
  <th>Actions</th> 
 </tr> 
 </thead> 
 <tbody>
  <?php 
  if (isset($_GET["initialDate"])){

    $initialDate=$_GET["initialDate"];
    $finalDate=$_GET["finalDate"];

  }else{

   $initialDate=null;
   $finalDate=null; 

  }
$response =SalesController::ctrSalesDatesRange($initialDate, $finalDate);

 foreach($response as $key => $value){
   echo ' <tr>
  <td>'.($key+1).'</td> 
  <td>'.$value["code"].'</td>';

  $itemClient = "id";
  $valueClient =$value["id_client"];
  $responseClient = ControllerClients::ctrShowClients($itemClient, $valueClient);


  echo'<td>'.$responseClient["name"].'</td>';
    $itemUser = "id";
  $valueUser =$value["id_seller"];
  $responseUser = UserController::ctrShowUsers($itemUser, $valueUser);
  echo '
  <td>'.$responseUser["name"].'</td> 
  <td>'.$value["payment_method"].'</td>
  <td>Kshs ' .number_format($value["net_price"],2).'</td>
  <td>Kshs ' .number_format($value["total"],2).'</td>
  <td>'.$value["date"].'</td> 
  <td>
  <div class="btn-group">
  <button class="btn btn-info btnPrintBill" paymethod="'.$value["payment_method"].'"  saleCode="'.$value["code"].'"><i class="fa fa-print"></i></button>';
 
 if ($_SESSION["profile"]=="administrator"){

  echo '<button class="btn btn-warning btnEditSales" idSales="'.$value["id"].'"><i class="fa fa-pencil"></i></button>
  <button class="btn btn-danger btnDeleteSale"  idSales="'.$value["id"].'"><i class="fa fa-times"></i></button>';
}
  echo '</div>
  </td> 
 </tr> ';
 }
    ?>
 </tbody>
</table>
<?php 

$deleteSales= new SalesController();
$deleteSales -> ctrDeleteSale();
 ?>
 
       
</div>
       
 </div>

</section>
 
</div>
