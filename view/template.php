<?php 

session_start();

 ?>
<!DOCTYPE html>
<html>
<head> 
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Quanta</title>
  <link rel="icon" href="view/img/template/icono-negro.png">
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="view/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="view/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="view/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="view/dist/css/AdminLTE.css">
  <!-- AdminLTE Skins. -->
  <link rel="stylesheet" href="view/dist/css/skins/_all-skins.min.css">

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <!-- DataTables -->
  <link rel="stylesheet" href="view/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- RESPNSIVE TABLE  -->
  <link rel="stylesheet" href="view/bower_components/datatables.net-bs/css/responsive.bootstrap.min.css">

  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="view/plugins/iCheck/all.css">

  <!-- Daterange picker -->
  <link rel="stylesheet" href="view/bower_components/bootstrap-daterangepicker/daterangepicker.css">

    <!-- Morris chart -->
  <link rel="stylesheet" href="view/bower_components/morris.js/morris.css">
  
  <!-- ENDS CSS STRT JS -->
  <!-- jQuery 3 -->
<script src="view/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="view/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="view/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="view/dist/js/adminlte.min.js"></script>
<!-- DataTables -->
<script src="view/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="view/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- responsive TABLE DATA -->
<script src="view/bower_components/datatables.net-bs/js/dataTables.responsive.min.js"></script>
<script src="view/bower_components/datatables.net-bs/js/responsive.bootstrap.min.js"></script>
<!-- SweetAlert vs2 -->
<script src="view/plugins/sweetalert2/sweetalert2.all.js"></script>
  <!-- iCheck 1.0.1 -->
 <script src="view/plugins/iCheck/icheck.min.js"></script>
   <!-- InputMask -->
  <script src="view/plugins/input-mask/jquery.inputmask.js"></script>
  <script src="view/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
  <script src="view/plugins/input-mask/jquery.inputmask.extensions.js"></script>
  <!-- JQuery number system plugin -->
  <script src="view/plugins/jqueryNumber/jquerynumber.min.js"></script>
  
   <!-- daterangepicker http://www.daterangepicker.com/-->
  <script src="view/bower_components/moment/min/moment.min.js"></script>
  <script src="view/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

    <!-- Morris.js charts http://morrisjs.github.io/morris.js/-->
  <script src="view/bower_components/raphael/raphael.min.js"></script>
  <script src="view/bower_components/morris.js/morris.min.js"></script>

 <!-- ChartJS http://www.chartjs.org/-->
  <script src="view/bower_components/chart.js/Chart.js"></script>
</head>

<body class="hold-transition skin-blue sidebar-mini login-page">
<!-- Site wrapper -->

<?php 
if (isset($_SESSION["beginSession"]) && $_SESSION["beginSession"]== "ok" ) {
  

echo '<div class="wrapper">';

include "modules/header.php";
include "modules/menu.php";

if (isset($_GET["root"])) {
  if($_GET["root"]=="home" ||
   $_GET["root"]=="users" ||
   $_GET["root"]=="categories" ||
   $_GET["root"]=="products" ||
   $_GET["root"]=="clients"||
   $_GET["root"]=="sales" ||
   $_GET["root"]=="create-sales"||
   $_GET["root"]=="edit-sales"||
   $_GET["root"]=="reports"||
   $_GET["root"]=="account"||
   $_GET["root"]=="logout"){
include "modules/".$_GET["root"].".php";
  }else{
    include "modules/404.php";
  }   
  
}else{
  include "modules/home.php";
}

include "modules/footer.php";

echo '</div>';
}else{
  include "modules/login.php";
}
 ?>
 <!-- ./wrapper -->
<script src="view/js/template.js"></script>
<script src="view/js/users.js"></script>
<script src="view/js/category.js"></script>
<script src="view/js/product.js"></script>
<script src="view/js/clients.js"></script>
<script src="view/js/sales.js"></script>
<script src="view/js/reports.js"></script>
<script src="view/js/salesAccountHandler.js"></script>
</body>
</html>
 