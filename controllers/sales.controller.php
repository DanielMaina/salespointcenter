<?php 

/**
 *GET TEMPLATE FROM VIEW TO INDEX..... 
 */
class SalesController{
	
	static public function ctrShowSales($item, $value){
		$table="sales";
		$response=SalesModel::mdlShowSales($table,$item,$value);
		return $response;
	}//ends saleController

	/*=============================================
	=            Section handle sales  v          =
	=============================================*/


static public function ctrCreateSale(){
	if (isset($_POST["newSales"])) {
		//handle the product  stock and increase sales
		$productsList = json_decode($_POST["productsList"],true);

		$totalPurchasedProducts = array();

		foreach ($productsList as $key => $value) {
			array_push($totalPurchasedProducts, $value["quantity"]);
			$tableProducts="products";
			$item ="id";
			$values=$value["id"];
			$order="id";
			$getProduct=ProductsModel::mdlShowProduct($tableProducts,$item,$values,$order);

			$item1a = "sales";
			$value1a = $value["quantity"] + $getProduct["sales"];

			 $newSales = ProductsModel::mdlUpdateProduct($tableProducts, $item1a, $value1a, $values);

				$item1b = "stock";
				$value1b = $value["stock"];

			$newStock = ProductsModel::mdlUpdateProduct($tableProducts, $item1b, $value1b, $values);


		}
		$tableCustomers = "clients";

			$item = "id";
			$valueCustomer = $_POST["selectClient"];

			$getCustomer = ModelClients::mdlShowClient($tableCustomers, $item, $valueCustomer);
			//var_dump($getCustomer["purchase"]);

			$item1a = "purchase";
			 $value1a = array_sum($totalPurchasedProducts) + $getCustomer["purchase"];

			$customerPurchases = ModelClients::mdlUpdateClient($tableCustomers, $item1a, $value1a, $valueCustomer);

			$item1b = "last_purchases";

			date_default_timezone_set('Africa/Nairobi');

			$date = date('Y-m-d');
			$hour = date('H:i:s');
			$value1b = $date.' '.$hour;

			$dateCustomer = ModelClients::mdlUpdateClient($tableCustomers, $item1b, $value1b, $valueCustomer);

			$table = "sales";
			$data = array("idSeller"=>$_POST["idSeller"],
						   "idClient"=>$_POST["selectClient"],
						   "code"=>$_POST["newSales"],
						   "products"=>$_POST["productsList"],
						   "tax"=>$_POST["newPriceTax"],
						   "netPrice"=>$_POST["newNetPrice"],
						   "totalPrice"=>$_POST["totalSale"],
						   "paymentMethod"=>$_POST["listPaymentMethod"]);
			$response = SalesModel::mdlAddSales($table,$data);
			         if ($response == "ok"){
					echo "<script>
					 swal({
					 	type:'success',
		                title: 'Success...',
		                text: 'Sales saved successfully',
		                showConfirmButton: true,
		                confirmButtonText:'Close',
		                closeOnConfirm:false
					 	}).then((result)=>{
					 		if(result.value){
					 			window.location = 'sales';
					 		}
					 		});
					 </script>";
				
		}
	}
}

/*==============================================
=            EDIT THE PRODUCT SALES            =
==============================================*/


static public function ctrEditSales(){
// 	$tablet="sales";
// 	$itemt ="id";
// 	$valuet=1157;
// 	$getSales=SalesModel::mdlShowSales($tablet,$itemt,$valuet);
// $productJsont=json_decode($getSales["products"], true);
// var_dump($productJsont);
		if (isset($_POST["editSales"])) {

	/*=============================================
	=            HADLE THE DATA IN THE SALES ALREADY =
	=============================================*/
    $table="sales";
	$item ="code";
	$value=$_POST["editSales"];
	$getSales=SalesModel::mdlShowSales($table,$item,$value);
    
	 /*========================================================
	 =   CHECK IF THERE IS PRODUCT IN THE EDIT LI            =
	 ========================================================*/

	 if($_POST["productsList"] == ""){

				$productsList = $getSales["products"];
				 $productChange = false;
			}else{
				$productsList = $_POST["productsList"];
				 $productChange = true;
			}

	if($productChange){
 
	 $productJson=json_decode($getSales["products"], true);	
	$totalPurchasedProducts = array();

	foreach ($productJson as $key => $value) {
       array_push($totalPurchasedProducts, $value["quantity"]);

	  $tableProducts="products";

      $item = "id";
	  $values = $value["id"];
	  $order="id";
	  //var_dump($value);
      $getProduct = ProductsModel::mdlShowProduct($tableProducts, $item, $values, $order);

      	$item1a = "sales";
		 $value1a = $getProduct["sales"] - $value["quantity"];
   //var_dump($value1a);
	        $newSales = ProductsModel::mdlUpdateProduct($tableProducts, $item1a, $value1a, $values);

       	$item1b = "stock";
		$value1b =$getProduct["stock"] + $value["quantity"];
		//var_dump($value1b);
        $newStock = ProductsModel::mdlUpdateProduct($tableProducts, $item1b, $value1b, $values);
         	


	}
	$tableClients = "clients";

	$itemClient = "id";
	$valueClient = $_POST["selectClient"];
    $getClient = ModelClients::mdlShowClient($tableClients, $itemClient, $valueClient);

     $item1a = "purchase";
     $value1a = $getClient["purchase"] - array_sum($totalPurchasedProducts);
	$customerPurchases = ModelClients::mdlUpdateClient($tableClients, $item1a, $value1a, $valueClient);

	/*=============================================
	=            UPDATE TO THE STOCK TABLE          =
	=============================================*/

		//handle the product  stock and increase sales
		$productsList_2 = json_decode($productsList,true);
        //var_dump($productsList_2);
		$totalPurchasedProducts = array();


		foreach ($productsList_2 as $key => $value) {
			array_push($totalPurchasedProducts, $value["quantity"]);
			$tableProducts="products";
			$item ="id";
			$values=$value["id"];
			$order="id";
			$getProduct=ProductsModel::mdlShowProduct($tableProducts,$item,$values,$order);

			$item1a = "sales";
			$value1a = $value["quantity"] + $getProduct["sales"];

			 $newSales = ProductsModel::mdlUpdateProduct($tableProducts, $item1a, $value1a, $values);

				$item1b = "stock";
				$value1b = $value["stock"];

			$newStock = ProductsModel::mdlUpdateProduct($tableProducts, $item1b, $value1b, $values);


		}
		$tableCustomers = "clients";

			$item = "id";
			$valueCustomer = $_POST["selectClient"];

			$getCustomer = ModelClients::mdlShowClient($tableCustomers, $item, $valueCustomer);
			//var_dump($getCustomer["purchase"]);

			$item1a = "purchase";
			 $value1a = array_sum($totalPurchasedProducts) + $getCustomer["purchase"];

			$customerPurchases = ModelClients::mdlUpdateClient($tableCustomers, $item1a, $value1a, $valueCustomer);

			$item1b = "last_purchases";

			date_default_timezone_set('Africa/Nairobi');

			$date = date('Y-m-d');
			$hour = date('H:i:s');
			$value1b = $date.' '.$hour;

			$dateCustomer = ModelClients::mdlUpdateClient($tableCustomers, $item1b, $value1b, $valueCustomer);

		}
          
          //SaveChanges of the sale...[]

			$data = array("idSeller"=>$_POST["idSeller"],
						   "idClient"=>$_POST["selectClient"],
						   "code"=>$_POST["editSales"],
						   "products"=>$productsList,
						   "tax"=>$_POST["newPriceTax"],
						   "netPrice"=>$_POST["newNetPrice"],
						   "totalPrice"=>$_POST["totalSale"],
						   "paymentMethod"=>$_POST["listPaymentMethod"]);
      //  var_dump($data);
			$response = SalesModel::mdlEditSales($table,$data);
			         if ($response == "ok"){
					echo "<script>
					 swal({
					 	type:'success',
		                title: 'Success...',
		                text: 'Sales Edited successfully',
		                showConfirmButton: true,
		                confirmButtonText:'Close',
		                closeOnConfirm:false
					 	}).then((result)=>{
					 		if(result.value){
					 			window.location = 'sales';
					 		}
					 		});
					 </script>";
				
		}
	 }
}
/*==========================================
=            DELETE THE SALESS!            =
==========================================*/

static public function ctrDeleteSale(){
	if (isset($_GET["idSales"])) {

		 $table ="sales";

		 $item="id";
		 $value= $_GET["idSales"];
		 $bringSales = SalesModel::mdlShowSales($table,$item,$value);
		/*=============================================
				=  UPDATade THE LAST SalES date    =
		=============================================*/
		$tableClinets ="clients";
		$itemSales= null;
		$valueSales= null;
		$getSales= SalesModel::mdlShowSales($table, $itemSales,$valueSales);

	$saveDates = array();

	foreach($getSales as $key => $value){
		if ($value["id_client"] == $bringSales["id_client"]) {
				
				array_push($saveDates,$value["date"]);	

				}	
			}
			if(count($saveDates) > 1){
				if($bringSales["id_client"] > $saveDates[count($saveDates)-2]){
					$item="last_purchases";
					$value=$saveDates[count($saveDates)-2];
					$valueIdClient=$bringSales["id_client"];
					$clientPurchases =ModelClients::mdlUpdateClient($tableClinets, $item, $value,$valueIdClient);
					
				}else{
					$item="last_purchases";
					$value=$saveDates[count($saveDates)-1];
					$valueIdClient=$bringSales["id_client"];
					$clientPurchases =ModelClients::mdlUpdateClient($tableClinets, $item, $value,$valueIdClient);
				}
			}else{
				$item ="last_purchases";
				$value ="0000-00-00 00:00:00";
				$valueIdClient=$bringSales["id_client"];
				$clientPurchases =ModelClients::mdlUpdateClient($tableClinets, $item, $value,$valueIdClient); 

			}
	/*=============================================
	 =            FORMAT THE PRODUCT TABLE            =
	 =============================================*/
 
	$productJson=json_decode($bringSales["products"], true);	
  
	$totalPurchasedProducts = array();

	foreach ($productJson as $key => $value) {
       array_push($totalPurchasedProducts, $value["quantity"]);

	  $tableProducts="products";

      $item = "id";
	  $values = $value["id"];
	  $order="id";
	  //var_dump($value);
      $getProduct = ProductsModel::mdlShowProduct($tableProducts, $item, $values,$order);

      	$item1a = "sales";
		 $value1a = $getProduct["sales"] - $value["quantity"];
   //var_dump($value1a);
	        $newSales = ProductsModel::mdlUpdateProduct($tableProducts, $item1a, $value1a, $values);

       	$item1b = "stock";
		$value1b =$getProduct["stock"] + $value["quantity"];
		//var_dump($value1b);
        $newStock = ProductsModel::mdlUpdateProduct($tableProducts, $item1b, $value1b, $values);
         	


	}
	$tableClients = "clients";

	$itemClient = "id";
	$valueClient =$bringSales["id_client"];
    $getClient = ModelClients::mdlShowClient($tableClients, $itemClient, $valueClient);

     $item1a = "purchase";
     $value1a = $getClient["purchase"] - array_sum($totalPurchasedProducts);
	$customerPurchases = ModelClients::mdlUpdateClient($tableClients, $item1a, $value1a, $valueClient);
/*====================================
=            DELETE MODEL            =
====================================*/
$response= SalesModel::mdlDleteSele($table,$_GET["idSales"]);
  if ($response == "ok"){ 
					echo "<>
					 swal({
					 	type:'success',
		                title: 'Success...',
		                text: 'Sales Deleted successfully',
		                showConfirmButton: true,
		                confirmButtonText:'Close',
		                closeOnConfirm:false
					 	}).then((result)=>{
					 		if(result.value){
					 			window.location = 'sales';
					 		}
					 		});
					 </>";
				
		}
	}
}
/*=============================================
	DATES RANGE
=============================================*/	

	static public function ctrSalesDatesRange($initialDate, $finalDate){

		$table = "sales";

		$answer = SalesModel::mdlSalesDatesRange($table, $initialDate, $finalDate);

		return $answer;
		
	}

	/*===========================================
	=            DOWNLOAD EXCEL WORK   ctrDownloadReport         =
	===========================================*/

public function ctrDownloadReport(){
	if (isset($_GET["reports"])) {
		$table= "sales";
		if (isset($_GET["initialDate"]) && isset($_GET["finalDate"])) {
			$sales =SalesModel::mdlSalesDatesRange($table, $_GET["initialDate"],$_GET["finalDate"]);
		}else{
			$item= null;
			$value= null;

           $sales =SalesModel::mdlShowSales($table, $item,$value);
		}

		 /*=========================================
		 =            CREATE EXCEL FILE            =
		 =========================================*/
		 date_default_timezone_set('Africa/Nairobi');
		 $date = date('Y-m-d');
		 $hour = date('H-i-s');

		 $name=$date.' '.$hour.'.xls';

		    header('Expires: 0');
			header('Cache-control: private');
			header("Content-type: application/vnd.ms-excel"); // Excel file
			header("Cache-Control: cache, must-revalidate"); 
			header('Content-Description: File Transfer');
			header('Last-Modified: '.date('D, d M Y H:i:s'));
			header("Pragma: public"); 
			header('Content-Disposition:; filename="'.$name.'"');
			header("Content-Transfer-Encoding: binary");

			echo utf8_decode("<table border='0'> 

					<tr> 
					<td style='font-weight:bold; border:1px solid #eee;'>Code</td> 
					<td style='font-weight:bold; border:1px solid #eee;'>customer</td>
					<td style='font-weight:bold; border:1px solid #eee;'>Seller</td>
					<td style='font-weight:bold; border:1px solid #eee;'>Quantity</td>
					<td style='font-weight:bold; border:1px solid #eee;'>Products</td>
					<td style='font-weight:bold; border:1px solid #eee;'>Buying Price</td>
					<td style='font-weight:bold; border:1px solid #eee;'>Selling Price</td>
					<td style='font-weight:bold; border:1px solid #eee;'>Profit/Prod</td>
					<td style='font-weight:bold; border:1px solid #eee;'>Profit/Sale</td>
					<td style='font-weight:bold; border:1px solid #eee;'>Charges</td>
					<td style='font-weight:bold; border:1px solid #eee;'>Net Price</td>		
					<td style='font-weight:bold; border:1px solid #eee;'>Total</td>		
					<td style='font-weight:bold; border:1px solid #eee;'>Payment</td>	
					<td style='font-weight:bold; border:1px solid #eee;'>Date</td>		
					</tr>
                    
					");
           foreach ($sales as $key => $item) {
           	$client = ControllerClients::ctrShowClients("id", $item["id_client"]);
           	$seller = UserController::ctrShowUsers("id", $item["id_seller"]);
           	echo utf8_decode("<tr>
                <td style='border:1px solid #eee;'>".$item["code"]."</td> 
                <td style='border:1px solid #eee;'>".$client["name"]."</td> 
                <td style='border:1px solid #eee;'>".$seller["name"]."</td>
                <td style='border:1px solid #eee;'>");

           	$products = json_decode($item["products"], true);

           	foreach ($products as $key => $valueproducts) {
           	echo utf8_decode($valueproducts["quantity"]."<br/>");
           	}
           	echo utf8_decode("</td><td style='border:1px solid #eee;'>");
           	foreach ($products as $key => $valueproducts) {
           		echo utf8_encode($valueproducts["description"]."<br/>");
           	}
			   echo utf8_encode("</td><td style='border:1px solid #eee;'>");
           	foreach ($products as $key => $valueproducts) {

				   $Buyingproduct=ProductsController::ctrShowProduct("id",$valueproducts["id"],"id");
				   echo utf8_encode($Buyingproduct["buying_price"]."<br/>");
           	}
			 echo utf8_encode("</td><td style='border:1px solid #eee;'>");
           	foreach ($products as $key => $valueproducts) {
           		echo utf8_encode($valueproducts["price"]."<br/>");
			   }
			 echo utf8_encode("</td><td style='border:10px solid #eee;'>");
			 foreach ($products as $key => $valueproducts) {
				$Buyingproduct=ProductsController::ctrShowProduct("id",$valueproducts["id"],"id");
				$profit=$valueproducts["price"]-$Buyingproduct["buying_price"];
				echo utf8_encode($profit*$valueproducts["quantity"]."<br/>");
			}  
			   echo utf8_encode("</td>
				   <td style='border:1px solid #eee;'>");
				   $final=0;
				   foreach ($products as $key => $valueproducts) {
					$Buyingproduct=ProductsController::ctrShowProduct("id",$valueproducts["id"],"id");
					$Totalprofit=$valueproducts["quantity"]*$Buyingproduct["buying_price"]; 
					$final +=$Totalprofit;
					$resultProfilt=$item["net_price"]-$final;
				}
				echo utf8_encode($resultProfilt."</td>
           		<td style='border:1px solid #eee;'>".number_format($item["tax"],2)."</td>
           		<td style='border:1px solid #eee;'>".number_format($item["net_price"],2)."</td>
           		<td style='border:1px solid #eee;'>".number_format($item["total"],2)."</td>
           		<td style='border:1px solid #eee;'>".$item["payment_method"]."</td>
           		<td style='border:1px solid #eee;'>".substr($item["date"],0,10)."</td>

           	 </tr>");
           }
			echo "</table>";
	}
}

/*===========================================
=            ctrAddingTotalSales            =
===========================================*/
static public function ctrAddingTotalSales(){
		$table = "sales";

		$answer = SalesModel::mdlAddingTotalSales($table);

		return $answer;
}

/*===========================================
=  24/11/20-          payDebt method           =
===========================================*/
static public function ctrUpdateDebt(){
	date_default_timezone_set('Africa/Nairobi');
	$date = date('Y-m-d');
	$hour = date('H:i:s');
	$updateTime=$date." ".$hour;
	if(isset($_POST["submitDebt"])){
		$table="payment_track";
		$data=["receipt_no"=>$_POST["ReceiptNo"],
			   "payment_method"=>$_POST["payType"],
			   "amount_paid"=>$_POST["moneyPaid"],
			   "remark"=>$_POST["remark"],
			   "pay_date"=>$updateTime];

$result=SalesModel::mdlUpdateDebt($table,$data);
if ($result== "ok"){
	echo "<>
	 swal({
		 type:'success',
		title: 'Success...',
		text: 'saved successfully',
		showConfirmButton: true,
		confirmButtonText:'Close',
		closeOnConfirm:false
		 }).then((result)=>{
			 if(result.value){
				 window.location = 'account';
			 }
			 });
	 </>";
}
	}
}//end insert debt method 

/*===========================================
=  24/11/20-          seeDebt method           =
===========================================*/
static public function ctrSeeDebt($item,$value){
$table="payment_track";
$response=SalesModel::mdlSeeDebt($table,$item,$value);
return $response;
}//end handle view Debt

/*===========================================
=  09/12/20-          insert expenses method           =
===========================================*/
static public function 	ctrPayExpenses(){

	date_default_timezone_set('Africa/Nairobi');
	$date = date('Y-m-d');
	$hour = date('H:i:s');
	$addTime=$date." ".$hour;
	if(isset($_POST["submitExpense"])){

$table="expenses";
$data=["amount"=>$_POST["expAmount"],
	   "remark"=>$_POST["expRemark"],
	   "pay_date"=>$addTime];
$expenseResult=SalesModel::mdlPayExpenses($table,$data);
if($expenseResult=="ok"){
	echo "<>
	swal({
		type:'success',
	   title: 'Success...',
	   text: 'saved successfully',
	   showConfirmButton: true,
	   confirmButtonText:'Close',
	   closeOnConfirm:false
		}).then((result)=>{
			if(result.value){
				window.location = 'account';
			}
			});
	</>";
}

}//end if isset
}//end ctrPayExpenses

static public function ctrSeeExpenses($item,$value){
	$table="expenses";
	$response=SalesModel::mdlSeeExpenses($table,$item,$value);
	return $response;
}//end handle view Debt

/*=============================================
CREATE EXCEL FILE FOR DEBTS 
=============================================*/
static public function ctrDownloadDebtReport(){
	$table="sales";
	$item= null;
	$value= null;
   $salesResponse =SalesModel::mdlShowSales($table, $item,$value);

   date_default_timezone_set('Africa/Nairobi');
   $date = date('Y-m-d');
   $hour=date('H-i-s');
   $name='debt '.$date.''.$hour.'.xls';

header('Expires: 0');
header('Cache-control: private');
header("Content-type: application/vnd.ms-excel"); // Excel file
header("Cache-Control: cache, must-revalidate"); 
header('Content-Description: File Transfer');
header('Last-Modified: '.date('D, d M Y H:i:s'));
header("Pragma: public"); 
header('Content-Disposition:; filename="'.$name.'"');
header("Content-Transfer-Encoding: binary");
echo utf8_decode("<table border='0'> 

<tr> 
<td style='font-weight:bold; border:1px solid #eee;'>R/I NO.</td> 
<td style='font-weight:bold; border:1px solid #eee;'>Customer</td>
<td style='font-weight:bold; border:1px solid #eee;'>Totals</td>
<td style='font-weight:bold; border:1px solid #eee;'>Paid</td>
<td style='font-weight:bold; border:1px solid #eee;'>Balance</td> </tr>");
foreach ($salesResponse as $key => $item) {

echo utf8_decode("<tr>
<td style='border:1px solid #eee;'>".$item["code"]."</td>
");

$itemClient = "id";
$valueClient =$item["id_client"];
$responseClient = ControllerClients::ctrShowClients($itemClient, $valueClient);	
echo utf8_decode('<td style="border:1px solid #eee;">'.$responseClient["name"].'</td>');
echo utf8_decode('<td style="border:1px solid #eee;" >'.number_format($item["total"],2).'</td>');

$receiptNo="receipt_no";
$receiptValue=$item["code"];
$result=SalesController::ctrSeeDebt($receiptNo,$receiptValue);
$totalP=0;
foreach($result as $key => $Invalue){ 
$totalP +=intval($Invalue["amount_paid"]);
}
$status=$item["total"]-$totalP;

echo utf8_decode('<td style="border:1px solid #eee;">'.$totalP.'</td><td style="border:1px solid #eee;">'.$status.'</td>');

}
echo "</tr> </table>";
}//end of download debt methods



}//end class


 ?>