<?php 

/**
 *GET TEMPLATE FROM VIEW TO IDEX 
 */
/**
 * client controller  class
 */
class ControllerClients{
	
static public  function ctrCreateClient(){
		if (isset($_POST["newName"])){
			if (preg_match('/^[a-zA-Z0-9 ]+$/',$_POST["newName"]) &&
			   preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/', $_POST["newEmail"])) {
			$table="clients";
			 $data= array(
			 	"name"=> $_POST["newName"],
			 	"documentId"=> $_POST["newDocumentId"],
			 	"email"=> $_POST["newEmail"],
			 	"phone"=> $_POST["newPhone"],
			 	"address"=> $_POST["newAddress"],
			 	"join_date"=> $_POST["newDategoin"],
			);
			 
			$response= ModelClients::mdlCreateClient($table,$data);
							if ($response == "ok"){
					echo "<script>
					 swal({
					 	type:'success',
		                title: 'Success...',
		                text: 'Client saved successfully',
		                showConfirmButton: true,
		                confirmButtonText:'Close',
		                closeOnConfirm:false
					 	}).then((result)=>{
					 		if(result.value){
					 			window.location = 'clients';
					 		}
					 		});
					 </script>";
				}

			 }else{
                 echo'<script>

					swal({
						  type: "error",
						  title: "Sorry please check if empty field or use  of special characters!",
						  showConfirmButton: true,
						  confirmButtonText: "Close"
						  }).then(function(result){
							if (result.value) {

							window.location = "clients";

							}
						})

			  	</script>';
			}
		}
	}//addcleint method ends

/*=====================================
	=            SHOW CUSTOMER            =
	=====================================*/
static public function ctrShowClients($item,$value){
$table="clients";
$response=ModelClients::mdlShowClient($table,$item,$value);

return $response;
}

/*===================================
=            EDIT CLIENT            =
===================================*/

static public  function ctrEditClient(){
		if (isset($_POST["editName"])){
			if (preg_match('/^[a-zA-Z0-9 ]+$/',$_POST["editName"]) && preg_match('/^[a-zA-Z0-9 ]+$/',$_POST["editDocumentId"]) &&
			   preg_match('/^[^0-9][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[@][a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,4}$/', $_POST["editEmail"])) {
			$table="clients";
			 $data= [
			 	"id"=>$_POST["idClient"],
			 	"name"=> $_POST["editName"],
			 	"documentId"=> $_POST["editDocumentId"],
			 	"email"=> $_POST["editEmail"],
			 	"phone"=> $_POST["editPhone"],
			 	"address"=> $_POST["editAddress"],
			 	"join_date"=> $_POST["editDategoin"],
			];

			$response= ModelClients::mdlEditClient($table,$data);
							if ($response == "ok"){
					echo "<script>
					 swal({
					 	type:'success',
		                title: 'Success...',
		                text: 'Client upadted successfully',
		                showConfirmButton: true,
		                confirmButtonText:'Close',
		                closeOnConfirm:false
					 	}).then((result)=>{
					 		if(result.value){
					 			window.location = 'clients';
					 		}
					 		});
					 </script>";
				}

			}else{
                 echo'<script>

					swal({
						  type: "error",
						  title: "Sorry please check if empty field or use  of special characters!",
						  showConfirmButton: true,
						  confirmButtonText: "Close"
						  }).then(function(result){
							if (result.value) {

							window.location = "clients";

							}
						})

			  	</script>';
			}
		}
	}//addcleint method ends

	/*====================================
	=            DELETECLIENT            =
	====================================*/
static public function	 ctrDeleteClient(){
	if (isset($_GET["idClient"])) {
		$table="clients";
		$data=$_GET['idClient'];

		$response= ModelClients::mdlDeleteClient($table,$data);
		if ($response == "ok"){
					echo "<script>
					 swal({
					 	type:'success',
		                title: 'Success...',
		                text: 'Client DELETED successfully',
		                showConfirmButton: true,
		                confirmButtonText:'Close',
		                closeOnConfirm:false
					 	}).then((result)=>{
					 		if(result.value){
					 			window.location = 'clients';
					 		}
					 		});
					 </script>";
				}


	}
}
}// end class





 ?>