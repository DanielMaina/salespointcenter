<?php 

/**
 *ENGINE CONTROLLER FOR THE CATEGORY 
 */
/**
 * 
 */
class CategoryController{

//use insert method.... 
 static public function ctrCreateCategory(){
if(isset($_POST["newCategory"])){
if (preg_match('/^[a-zA-Z0-9 ]+$/', $_POST["newCategory"])){
   $table="categories";
   $data=$_POST["newCategory"];
   $response = CaterogyModel::mdlCreateCategory($table, $data);
   if ($response =="ok") {
   	 echo "<script>
			 swal({
			 	type:'success',
                text: 'Category Added Successfully!',
                showConfirmButton: true,
                confirmButtonText:'Close',
                closeOnConfirm:false
			 	}).then((result)=>{
			 		if(result.value){
			 			window.location = 'categories';
			 		}
			 		});
			 </script>";	 
   }
	}else{
		echo "<script>
			 swal({
			 	type:'error',
                title: 'Oops...',
                text: 'Error! category should not be empty or with special character!',
                showConfirmButton: true,
                confirmButtonText:'Close',
                closeOnConfirm:false
			 	}).then((result)=>{
			 		if(result.value){
			 			window.location = 'categories';
			 		}
			 		});
			 </script>";	
	}
}

 }//END METHOD ADDCATEGORY...
 /*============================================
 	=            SELECT FROM CATEGORY            =
 	============================================*/
 	
static public function crtShowCategory($item,$value) {
$table="categories";
$response =CaterogyModel::mdlShowCategory($table,$item,$value);
return $response;
}	
/*=====================================
=            EDIT CATEGORY            =
=====================================*/
static public function  ctrEditCategory(){
if(isset($_POST["editCategory"])){
if (preg_match('/^[a-zA-Z0-9 ]+$/', $_POST["editCategory"])){
   $table="categories";
   $data=array(
   	"id" => $_POST["idCategory"],
   	"category" => $_POST["editCategory"]
   );
   $response = CaterogyModel::mdlEditCategory($table, $data);
   if ($response =="ok") {
   	 echo "<script>
			 swal({
			 	type:'success',
                text: 'Category update Successfully!',
                showConfirmButton: true,
                confirmButtonText:'Close',
                closeOnConfirm:false
			 	}).then((result)=>{
			 		if(result.value){
			 			window.location = 'categories';
			 		}
			 		});
			 </script>";	 
   }
	}else{
		echo "<script>
			 swal({
			 	type:'error',
                title: 'Oops...',
                text: 'Error! for edit empty not allowed!',
                showConfirmButton: true,
                confirmButtonText:'Close',
                closeOnConfirm:false
			 	}).then((result)=>{
			 		if(result.value){
			 			window.location = 'categories';
			 		}
			 		});
			 </script>";	
	}	
}

}

/*=======================================
=            DELETE CATEGORY            =
=======================================*/
static public function ctrDeleteCategory(){
 if (isset($_GET["idCategory"])) {
 	$table="categories";
 	$data=$_GET["idCategory"];
 	$response= CaterogyModel::mdlDeleteCategory($table, $data);
 	if ($response =="ok") {
 	 echo "<script>
			 swal({
			 	type:'success',
                text: 'Category Deleted Successfully!',
                showConfirmButton: true,
                confirmButtonText:'Close',
                closeOnConfirm:false
			 	}).then((result)=>{
			 		if(result.value){
			 			window.location = 'categories';
			 		}
			 		});
			 </script>";	 
 	}
 	}	
}
}//END CLASS;;;

 ?>