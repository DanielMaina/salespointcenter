<?php 

/**
 *GET TEMPLATE FROM VIEW TO IDEX 
 */
/**
 * 
 */
class UserController
{
	
static public function ctrLoginUser(){
		if (isset($_POST["username"])) {
			if (preg_match('/^[a-zA-Z0-9]+$/', $_POST["username"]) && preg_match('/^[a-zA-Z0-9]+$/', $_POST["password"])){

				$encrypt = crypt($_POST["password"],'$2a$07$usesomesillystringforsalt$');
				$table="users";

				$item="username";
				$value=$_POST["username"];

				$response = ModelUsers::mdlModelUsers($table,$item,$value);

				if($response["username"] == $_POST["username"] && $response["password"] == $encrypt){
					if($response["status"] == 1) {
						
					$_SESSION["beginSession"]="ok";
					$_SESSION["id"]=$response["id"];
					$_SESSION["name"]=$response["name"];
					$_SESSION["username"]=$response["username"];
					$_SESSION["photo"]=$response["photo"];
					$_SESSION["profile"]=$response["profile"];


					//Handle the last_Login...
					date_default_timezone_set("Africa/Nairobi");
					$year=date("Y-m-d");
					$hour=date("H:i:s");
					$fetchDate = $year.' '.$hour; 
					$item1="last_login";
					$value1=$fetchDate;
					$item2= "id";
					$value2=$response["id"];

					$lastLogin= ModelUsers::mdlUpdateUser($table,$item1,$value1,$item2,$value2);
					if ($lastLogin =="ok") {
					echo "<script>window.open('home','_self')</script>";	
					}
				     }else{
				     	echo '<br/><div class="alert alert-danger"><strong>Error! </strong> Cannot Login You are not  Activated</div>';
				     }
				}else{
					echo '<br/><div class="alert alert-danger"><strong>Error! </strong> Cannot Login Check Credential and ReTry</div>';
				}
				
			}
		}
	}

	/*=============================================
	=            Register Users             =
	=============================================*/
	static public function ctrCreateNewUser(){
		if (isset($_POST["submit"])){
			if (preg_match('/^[a-zA-Z0-9 ]+$/',$_POST["newName"]) && preg_match('/^[a-zA-Z0-9]+$/', $_POST["newUser"]) && preg_match('/^[a-zA-Z0-9]+$/',$_POST["newPassword"])) {
      
      // validate Image
				$root= ""; 
				if(isset($_FILES["newPhoto"]["tmp_name"])){
				list($width, $height)= getimagesize($_FILES["newPhoto"]["tmp_name"]);
				 
				$newWidth=500;
				$newHieght=500;

				// create the folder of photos
				$directory = "view/img/users/".$_POST["newUser"];
				mkdir($directory, 0777);

				if($_FILES["newPhoto"]["type"] == "image/png"){
					$numberRandom=mt_rand(100,999);
					$root="view/img/users/".$_POST["newUser"]."/".$numberRandom.".png";
					$origin= imagecreatefrompng($_FILES["newPhoto"]["tmp_name"]);
					$destination=imagecreatetruecolor($newWidth,$newHieght);
					imagecopyresized($destination,$origin,0,0,0,0,$newWidth,$newHieght,$width,$height);
					imagepng($destination,$root);
				}

				if($_FILES["newPhoto"]["type"] == "image/jpeg"){
					$numberRandom=mt_rand(100,999);
					$root="view/img/users/".$_POST["newUser"]."/".$numberRandom.".jpg";
					$origin= imagecreatefromjpeg($_FILES["newPhoto"]["tmp_name"]);
					$destination=imagecreatetruecolor($newWidth,$newHieght);
					imagecopyresized($destination,$origin,0,0,0,0,$newWidth,$newHieght,$width,$height);
					imagejpeg($destination,$root);
				}


				}

				$table ="users";
				$status="0";

				$encrypt = crypt($_POST["newPassword"],'$2a$07$usesomesillystringforsalt$');
				$data=array(
				 "name" => $_POST["newName"],
				 "username"=>$_POST["newUser"], 
				 "password"=> $encrypt,
				 "profile"=>$_POST["newProfile"],
				 "photo"=>$root,
				 "status"=>$status); 
				
				

				$response = ModelUsers::mdlAddUser($table,$data);

				if ($response == "ok"){
					echo "<script>
					 swal({
					 	type:'success',
		                title: 'Success...',
		                text: 'User saved successfully',
		                showConfirmButton: true,
		                confirmButtonText:'Close',
		                closeOnConfirm:false
					 	}).then((result)=>{
					 		if(result.value){
					 			window.location = 'users';
					 		}
					 		});
					 </script>";
				}
				
			}else{  
			echo "<script>
			 swal({
			 	type:'error',
                title: 'Oops...',
                text: 'Error Field should not be empty or with special character!',
                showConfirmButton: true,
                confirmButtonText:'Close',
                closeOnConfirm:false
			 	}).then((result)=>{
			 		if(result.value){
			 			window.location = 'users';
			 		}
			 		});
			 </script>";	
			}
		}
	}

/*=============================================
=            Controller method that handle view User
=============================================*/
	static public function ctrShowUsers($item,$value){
		$table= "users"; 
		$response = ModelUsers::mdlModelUsers($table,$item,$value);
		return $response;
	}
	/*=============================================
	=           EDIT USER Details          =
	=============================================*/

static public function ctrEditUser(){
 if (isset($_POST["editUser"])) {
	if(preg_match('/^[a-zA-Z0-9 ]+$/',$_POST["editName"])) {


		//VALIDATE USER::: 
		$root =$_POST["currentPhoto"]; 
		if(isset($_FILES["editPhoto"]["tmp_name"]) && !empty($_FILES["editPhoto"]["tmp_name"])){
				list($width, $height)= getimagesize($_FILES["editPhoto"]["tmp_name"]);
				 
				$newWidth=500;
				$newHieght=500;

				// create the folder of photos
				$directory = "view/img/users/".$_POST["editUser"];
				if (!empty($_POST["currentPhoto"])) {
					unlink($_POST["currentPhoto"]);
				}else{
				mkdir($directory, 0777);
			}

				if($_FILES["editPhoto"]["type"] == "image/png"){
					$numberRandom=mt_rand(100,999);
					$root="view/img/users/".$_POST["editUser"]."/".$numberRandom.".png";
					$origin= imagecreatefrompng($_FILES["editPhoto"]["tmp_name"]);
					$destination=imagecreatetruecolor($newWidth,$newHieght);
					imagecopyresized($destination,$origin,0,0,0,0,$newWidth,$newHieght,$width,$height);
					imagepng($destination,$root);
				}

				if($_FILES["editPhoto"]["type"] == "image/jpeg"){
					$numberRandom=mt_rand(100,999);
					$root="view/img/users/".$_POST["editUser"]."/".$numberRandom.".jpg";
					$origin= imagecreatefromjpeg($_FILES["editPhoto"]["tmp_name"]);
					$destination=imagecreatetruecolor($newWidth,$newHieght);
					imagecopyresized($destination,$origin,0,0,0,0,$newWidth,$newHieght,$width,$height);
					imagejpeg($destination,$root);
				}


				}

		$table = "users";
        if ($_POST["editPassword"] != ""){
         if (preg_match('/^[a-zA-Z0-9]+$/', $_POST["editPassword"])) {
         	
        	$encrypt = crypt($_POST["editPassword"],'$2a$07$usesomesillystringforsalt$');
        }else{
        echo "<script>
			 swal({
			 	type:'error',
                title: 'Oops...',
                text: 'Error Password Field should not be empty or with special character!',
                showConfirmButton: true,
                confirmButtonText:'Close',
                closeOnConfirm:false
			 	}).then((result)=>{
			 		if(result.value){
			 			window.location = 'users';
			 		}
			 		});
			 </script>";		
        }
        }else{
        $encrypt = $_POST["currentPassword"];
    }
         $data=array(
				 "name" => $_POST["editName"],
				 "username"=>$_POST["editUser"], 
				 "password"=> $encrypt,
				 "profile"=>$_POST["editProfile"],
				 "photo"=>$root); 
         $response = ModelUsers::mdlEditUser($table,$data);
   if ($response == "ok"){
					echo "<script>
					 swal({
					 	type:'success',
		                title: 'Success...',
		                text: 'User updated successfully',
		                showConfirmButton: true,
		                confirmButtonText:'Close',
		                closeOnConfirm:false
					 	}).then((result)=>{
					 		if(result.value){
					 			window.location = 'users';
					 		}
					 		});
					 </script>";
				}else{
   	echo "<script>
			 swal({
			 	type:'error',
                title: 'Oops...',
                text: 'Error name field should not be empty or with special character!',
                showConfirmButton: true,
                confirmButtonText:'Close',
                closeOnConfirm:false
			 	}).then((result)=>{
			 		if(result.value){
			 			window.location = 'users';
			 		}
			 		});
			 </script>";	

     }
    }
   }
  }

/*===================================
=            Delete user            =
===================================*/

static public  function ctrDeleteUser(){
if(isset($_GET["idUser"])){
  $table="users";
  $data= $_GET['idUser'];
  if (isset($_GET["userphoto"])) {
 unlink($_GET["userphoto"]); 
 rmdir('view/img/users/'.$_GET["username"]);	
  }
  $response=ModelUsers::mdlDeleteUser($table,$data);
  if ($response == "ok"){
		echo "<script>
		swal({
		type:'success',
		title: 'Success...',
		text: 'User updated successfully',
		showConfirmButton: true,
		confirmButtonText:'Close',
		closeOnConfirm:false
		}).then((result)=>{
		if(result.value){
		window.location = 'users';
		}
		});
	</script>";
	}
 }

}

}
 ?>