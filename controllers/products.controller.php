<?php 
/**
 *PRODUCT CONTROLLER
 */
class ProductsController {
	/*===================================
	=            showPRODUCT            =
	===================================*/
static public function ctrShowProduct($item, $value,$order){
$table="products";
$response =ProductsModel::mdlShowProduct($table,$item,$value,$order);
return $response;
}
/*=====================================
=            CREATEPRODUCT            =
=====================================*/

static public function ctrCreateProduct(){
	if(isset($_POST["newDescription"])) {
	 if(preg_match('/^[0-9. ]+$/', $_POST["newBuyingPrice"])&&
        preg_match('/^[0-9. ]+$/', $_POST["newSellingPrice"]) ) {

	 	/*======================================
	 	=            VALIDATE IMAGE            =
	 	======================================*/
	 	
	 	$root="view/img/products/default/anonymous.png";
	        // validate Image
			 
				if(isset($_FILES["newImage"]["tmp_name"])){
				list($width, $height)= getimagesize($_FILES["newImage"]["tmp_name"]);
				 
				$newWidth=500;
				$newHieght=500;

				// create the folder of photos
				$directory = "view/img/products/".$_POST["newCode"];
				mkdir($directory, 0777);

				if($_FILES["newImage"]["type"] == "image/png"){
					$numberRandom=mt_rand(100,999);
					$root="view/img/products/".$_POST["newCode"]."/".$numberRandom.".png";
					$origin= imagecreatefrompng($_FILES["newImage"]["tmp_name"]);
					$destination=imagecreatetruecolor($newWidth,$newHieght);
					imagecopyresized($destination,$origin,0,0,0,0,$newWidth,$newHieght,$width,$height);
					imagepng($destination,$root);
				}

				if($_FILES["newImage"]["type"] == "image/jpeg"){
					$numberRandom=mt_rand(100,999);
					$root="view/img/products/".$_POST["newCode"]."/".$numberRandom.".jpg";
					$origin= imagecreatefromjpeg($_FILES["newImage"]["tmp_name"]);
					$destination=imagecreatetruecolor($newWidth,$newHieght);
					imagecopyresized($destination,$origin,0,0,0,0,$newWidth,$newHieght,$width,$height);
					imagejpeg($destination,$root);
				}


				}
		 $table="products";
        $data = array("id_category" => $_POST["newCategory"],
        	           "code" => $_POST["newCode"],
        	           "description" => $_POST["newDescription"],
        	           "stock" => $_POST["newStock"],
        	           "buying_price" => $_POST["newBuyingPrice"],
        	           "selling_price" => $_POST["newSellingPrice"],
        	           "sales"=>'0',
        	           "image" => $root);
        $response=ProductsModel::mdlCreateProduct($table,$data);
         if ($response == "ok"){
					echo "<script>
					 swal({
					 	type:'success',
		                title: 'Success...',
		                text: 'Product saved successfully',
		                showConfirmButton: true,
		                confirmButtonText:'Close',
		                closeOnConfirm:false
					 	}).then((result)=>{
					 		if(result.value){
					 			window.location = 'products';
					 		}
					 		});
					 </script>";
				}
		}else{
         echo "<script>
					 swal({
					 	type:'error',
		                title: 'Oops!',
		                text: 'Field cannot be empty or with special charactors',
		                showConfirmButton: true,
		                confirmButtonText:'Close',
		                closeOnConfirm:false
					 	}).then((result)=>{
					 		if(result.value){
					 			window.location = 'products';
					 		}
					 		});
					 </script>";
				}
		}	
	}


/*=====================================
=            EDITPRODUCT            =
=====================================*/

static public function ctrEditProduct(){
	if(isset($_POST["editDescription"])) {
	 if(preg_match('/^[0-9. ]+$/', $_POST["editBuyingPrice"])&&
        preg_match('/^[0-9. ]+$/', $_POST["editSellingPrice"]) ) {

	 	/*======================================
	 	=            VALIDATE IMAGE            =
	 	======================================*/
	 	
	 	$root=$_POST["actualImage"];
	        // validate Image
			 
				if(isset($_FILES["editImage"]["tmp_name"]) && !empty($_FILES["editImage"]["tmp_name"])){
				list($width, $height)= getimagesize($_FILES["editImage"]["tmp_name"]);
				 
				$newWidth=500;
				$newHieght=500;

				// create the folder of photos
				$directory = "view/img/products/".$_POST["editCode"];
				if (!empty($_POST["actualImage"])&& $_POST["actualImage"] !="view/img/products/default/anonymous.png") {
					unlink($_POST["actualImage"]);
				}else{
				mkdir($directory, 0777);
			    }

				if($_FILES["editImage"]["type"] == "image/png"){
					$numberRandom=mt_rand(100,999);
					$root="view/img/products/".$_POST["editCode"]."/".$numberRandom.".png";
					$origin= imagecreatefrompng($_FILES["editImage"]["tmp_name"]);
					$destination=imagecreatetruecolor($newWidth,$newHieght);
					imagecopyresized($destination,$origin,0,0,0,0,$newWidth,$newHieght,$width,$height);
					imagepng($destination,$root);
				}

				if($_FILES["editImage"]["type"] == "image/jpeg"){
					$numberRandom=mt_rand(100,999);
					$root="view/img/products/".$_POST["editCode"]."/".$numberRandom.".jpg";
					$origin= imagecreatefromjpeg($_FILES["editImage"]["tmp_name"]);
					$destination=imagecreatetruecolor($newWidth,$newHieght);
					imagecopyresized($destination,$origin,0,0,0,0,$newWidth,$newHieght,$width,$height);
					imagejpeg($destination,$root);
				}


				}
		 $table="products";
        $data = array("id_category" => $_POST["editCategory"],
					   "code" => $_POST["editCode"],
					   "id" => $_POST["editId"],
					   "sales" => $_POST["editSales"],
        	           "description" => $_POST["editDescription"],
        	           "stock" => $_POST["editStock"],
        	           "buying_price" => $_POST["editBuyingPrice"],
        	           "selling_price" => $_POST["editSellingPrice"],
					   "image" => $root);		   
        $response=ProductsModel::mdlEditProduct($table,$data);
         if ($response == "ok"){
					echo "<script>
					 swal({
					 	type:'success',
		                title: 'Success...',
		                text: 'Product Edited successfully',
		                showConfirmButton: true,
		                confirmButtonText:'Close',
		                closeOnConfirm:false
					 	}).then((result)=>{
					 		if(result.value){
					 			window.location = 'products';
					 		}
					 		});
					 </script>";
				}
		}else{
         echo "<script>
					 swal({
					 	type:'error',
		                title: 'Oops!',
		                text: 'Field cannot be empty or with special charactors',
		                showConfirmButton: true,
		                confirmButtonText:'Close',
		                closeOnConfirm:false
					 	}).then((result)=>{
					 		if(result.value){
					 			window.location = 'products';
					 		}
					 		});
					 </script>";
				}
		}	
	}
/*=========================================
=            DELETE PRODUCT!!!            =
=========================================*/
static public function ctrDeleteProduct(){
if(isset($_GET["idProduct"])) {
	$table="products";
	$data=$_GET["idProduct"];
	if ($_GET["image"] != "" && $_GET["image"] !="view/img/products/default/anonymous.png"){
		unlink($_GET["image"]);
		rmdir('view/img/products/'.$_GET["code"]);

	}
	$response = $response=ProductsModel::mdlDeleteProduct($table,$data);
	         if ($response == "ok"){
					echo "<script>
					 swal({
					 	type:'success',
		                title: 'Success...',
		                text: 'Product Deleted successfully',
		                showConfirmButton: true,
		                confirmButtonText:'Close',
		                closeOnConfirm:false
					 	}).then((result)=>{
					 		if(result.value){
					 			window.location = 'products';
					 		}
					 		});
					 </script>";
				}

	
}

}
	/*=============================================
	SHOW ADDING OF THE SALES ctrDownloadProduct
	=============================================*/

	static public function ctrShowAddingOfTheSales(){

		$table = "products";

		$answer = ProductsModel::mdlShowAddingOfTheSales($table);

		return $answer;

	}

	/*=============================================
	CREATE EXCEL FILE FOR PRODUCT 
	=============================================*/
static public function ctrDownloadProduct(){

$item=null;
$value=null;
$order=1;	
$table="products";
$productResp =ProductsModel::mdlShowProduct($table,$item,$value,$order);
date_default_timezone_set('Africa/Nairobi');
$date = date('Y-m-d');
$hour=date('H-i-s');
$name='product '.$date.''.$hour.'.xls';

header('Expires: 0');
header('Cache-control: private');
header("Content-type: application/vnd.ms-excel"); // Excel file
header("Cache-Control: cache, must-revalidate"); 
header('Content-Description: File Transfer');
header('Last-Modified: '.date('D, d M Y H:i:s'));
header("Pragma: public"); 
header('Content-Disposition:; filename="'.$name.'"');
header("Content-Transfer-Encoding: binary");

echo utf8_decode("<table border='0'> 

<tr> 
<td style='font-weight:bold; border:1px solid #eee;'>Name</td> 
<td style='font-weight:bold; border:1px solid #eee;'>Category</td>
<td style='font-weight:bold; border:1px solid #eee;'>Stock</td>
<td style='font-weight:bold; border:1px solid #eee;'>Sales</td>
<td style='font-weight:bold; border:1px solid #eee;'>Buying Price</td>
<td style='font-weight:bold; border:1px solid #eee;'>Selling Price Price</td>
<td style='font-weight:bold; border:1px solid #eee;'>Date</td>	
<td style='font-weight:bold; border:1px solid #eee;'>Image</td>
</tr>
");
foreach($productResp as $key => $item){
   $categoryResp=CategoryController::crtShowCategory("id",$item["id_category"]);
   echo utf8_encode("<tr>
	<td style='border:1px solid #eee;'>".$item["description"]."</td>
	<td style='border:1px solid #eee;'>".$categoryResp["category"]."</td>
	<td style='border:1px solid #eee;'>".$item["stock"]."</td>
	<td style='border:1px solid #eee;'>".$item["sales"]."</td>
	<td style='border:1px solid #eee;'>".$item["buying_price"]."</td>
	<td style='border:1px solid #eee;'>".$item["selling_price"]."</td>
	<td style='border:1px solid #eee;'>".$item["date"]."</td>
	<td style='border:1px solid #eee;'><img src='view/img/products/default/anonymous.png'/></td>
   </tr>");
}
   echo "</table>";
}


}//end of class productcontroller
 ?>