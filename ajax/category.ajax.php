<?php 
require_once "../controllers/categories.controller.php";
require_once "../models//categories.model.php";
/**
 * 
 */
class AjaxCategory{
	
	public $idCategory;

	public function ajaxEditCategory(){
     $item="id";
     $value = $this->idCategory;
     $response=CategoryController::crtShowCategory($item,$value);
     echo json_encode($response);
	}
	
}

// EDIT THE CATEGORY OBJECT
if (isset($_POST["idCategory"])){
	$category= new AjaxCategory();
	$category-> idCategory=$_POST["idCategory"];
	$category->ajaxEditCategory();
}
 ?>