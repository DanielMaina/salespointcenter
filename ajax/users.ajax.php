<?php 
require_once "../controllers/users.controller.php";
require_once "../models//users.model.php";


class AjaxUsers{
/*=================================
=            Edit User            =
=================================*/

public $idUser;

 public function ajaxEditUser(){

$item="id";
$value=$this->idUser;
$response = UserController::ctrShowUsers($item, $value);
echo json_encode($response); 

}
/*=============================================
=           ACTIVATE USER           =
=============================================*/
public $activatId;
public $activatUser;

 public function ajaxActivateUser(){
$table ="users";

$item1 ="status";
$value1=$this->activatUser;

$item2 ="id";
$value2=$this->activatId;
$response = ModelUsers::mdlUpdateUser($table,$item1,$value1,$item2,$value2);
}

// VALIDATE IF THE USER IS ALREADY THERE!!
public $validateUsername;
public function ajaxValidateUsername(){

$item="username";
$value=$this->validateUsername;
$response = UserController::ctrShowUsers($item, $value);
echo json_encode($response); 

}

}
// CREATE OBJECT HANDLE INSTANCE OF ABOCVE CLASS...
if (isset($_POST["idUser"])) {
	
$edit = new AjaxUsers();
$edit-> idUser = $_POST["idUser"];
$edit->ajaxEditUser();

}

//ACTIVATE USER

if (isset($_POST["activatUser"])) {
 	$activatUser = new AjaxUsers();
 	$activatUser -> activatUser = $_POST["activatUser"];
 	$activatUser -> activatId = $_POST["activatId"];
 	$activatUser->ajaxActivateUser();
 } 

 // ValidateUserObject

 if (isset($_POST["validateUsername"])) {
 	$validate=new AjaxUsers();
 	$validate -> validateUsername=$_POST['validateUsername'];
 	$validate-> ajaxValidateUsername();
 }