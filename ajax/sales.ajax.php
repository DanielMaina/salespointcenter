<?php
require_once "../controllers/sales.controller.php";
require_once "../models/sales.model.php";

class SalesNoValidate{
    public $validateRINo;

    public function SalesNoValidateFn(){
        $itemNo="code";
        $valueNo=$this->validateRINo;
        $resultNo=SalesController::ctrShowSales($itemNo,$valueNo);
        echo json_encode($resultNo);
    }
}
///instantiate class and method 
if(isset($_POST["validateRINo"])){
 $validateNO=new SalesNoValidate();
 $validateNO -> validateRINo=$_POST['validateRINo'];
 $validateNO -> SalesNoValidateFn();
}