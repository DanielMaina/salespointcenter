<?php 
/**
 * GET DATA USINGDYNAMIC DATATABLE PLUGIN
 */
require_once "../controllers/products.controller.php";
require_once "../models/products.model.php";
class TableProductsSales{
  
public function showProductsSales(){
    $item = null;
    $value = null;
    $order = "id";

    $products = ProductsController::ctrShowProduct($item, $value, $order);

 $dataJson = '{
  "data": [';

  for ($i=0; $i < count($products); $i++){ 

   $image ="<img src='".$products[$i]["image"]."' width='40px'>";

    if($products[$i]["stock"] <= 10) {
      $stock="<button class='btn btn-danger'>".$products[$i]["stock"]."</button>";
    }elseif ($products[$i]["stock"] > 11 && $products[$i]["stock"] <=15) {
       $stock="<button class='btn btn-warning'>".$products[$i]["stock"]."</button>";
     }else{
   $stock="<button class='btn btn-success'>".$products[$i]["stock"]."</button>";
 }

   $button ="<div class='btn-group'><button class='btn btn-success addProduct recoverButton' idProduct='".$products[$i]["id"]."'>Add</button> </div>";

   $dataJson .= '[
      "'.($i+1).'",
      "'.$image.'",
      "'.$products[$i]["code"].'",
      "'.$products[$i]["description"].'",
      "'.$stock.'",
      "'.$button.'"
    ],';

  }
  $dataJson = substr($dataJson, 0,-1);
   $dataJson .=  ']
}';
   echo $dataJson;  

  }
}
// create object to
$activateProductsSales = new TableProductsSales();
$activateProductsSales -> showProductsSales();
 ?>