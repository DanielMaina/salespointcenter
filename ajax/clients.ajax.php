<?php 
require_once "../controllers/clients.controller.php";
require_once "../models/clients.model.php";

/**
 * ajax endt customer
 */
class AjaxClients{
 public $idClient;
	public function  AjaxEditClient(){

	$item = "id";
	$value = $this -> idClient;
	$resposne=ControllerClients::ctrShowClients($item,$value);

	echo json_encode($resposne);
	
	}
}
/*===========================================
=            object of the class            =
===========================================*/

if (isset($_POST["idClient"])) {
	$clients = new AjaxClients();
	$clients -> idClient = $_POST["idClient"];
	$clients -> AjaxEditClient();

}