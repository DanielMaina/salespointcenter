<?php 

/**
 * GET DATA USINGDYNAMIC DATATABLE PLUGIN
 */
require_once "../controllers/products.controller.php";
require_once "../models/products.model.php";
require_once "../controllers/categories.controller.php";
require_once "../models/categories.model.php";
class TableProducts{
	
public function showProducts(){
    $item = null;
    $value = null;
    $order = "id";

    $products = ProductsController::ctrShowProduct($item, $value, $order);

 $dataJson = '{
  "data": [';

  for ($i=0; $i < count($products); $i++){ 

   $image ="<img src='".$products[$i]["image"]."' width='40px'>";

   $item = "id";
   $value = $products[$i]["id_category"];
   $category=CategoryController::crtShowCategory($item, $value);
   
    if($products[$i]["stock"] <= 10) {
      $stock="<button class='btn btn-danger'>".$products[$i]["stock"]."</button>";
    }elseif ($products[$i]["stock"] > 11 && $products[$i]["stock"] <=15) {
       $stock="<button class='btn btn-warning'>".$products[$i]["stock"]."</button>";
     }else{
   $stock="<button class='btn btn-success'>".$products[$i]["stock"]."</button>";
 }
  if (isset($_GET["hiddenProfile"]) && $_GET["hiddenProfile"] == "special") {
     $button ="<div class='btn-group'><button class='btn btn-warning btnEditProduct' idProduct='".$products[$i]["id"]."' data-toggle='modal' data-target='#modalEditProduct'><i class='fa fa-pencil'></i></button></div>";
   
  }else{
   $button ="<div class='btn-group'><button class='btn btn-warning btnEditProduct' idProduct='".$products[$i]["id"]."' data-toggle='modal' data-target='#modalEditProduct'><i class='fa fa-pencil'></i></button><button class='btn btn-danger btnDeleteProduct' idProduct='".$products[$i]["id"]."' code='".$products[$i]["code"]."' image='".$products[$i]["image"]."'><i class='fa fa-times'></i></button></div>";
  }
 

   $dataJson .= '[
      "'.($i+1).'",
      "'.$image.'",
      "'.$products[$i]["code"].'",
      "'.$products[$i]["description"].'",
      "'.$category["category"].'",
      "'.$stock.'",
      "'.$products[$i]["buying_price"].'",
      "'.$products[$i]["selling_price"].'",
      "'.$products[$i]["date"].'",
      "'.$button.'"
    ],';

  }
  $dataJson = substr($dataJson, 0,-1);
   $dataJson .=  ']
}';
   echo $dataJson;	

	}
}
// create object to
$activateProducts = new TableProducts();
$activateProducts -> showProducts();
 ?>