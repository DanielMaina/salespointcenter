<?php 
require_once "../controllers/products.controller.php";
require_once "../models/products.model.php";

/**
 * GET AJAX PRODUCT CODE METHOD::
 */
class AjaxProduct{
	public $idCategory;  
	public function ajaxCreateCodeProduct(){
		$item = "id_category";
		$value = $this -> idCategory;
		$order = "id";
      $response=ProductsController::ctrShowProduct($item, $value, $order);
     echo json_encode($response);
	}

/*====================================
=            EDIT PRODUCT            =
====================================*/
public $idProduct;
public $bringProduct;
public $nameProduct;

public function ajaxEditProduct(){
if ($this->bringProduct == "ok") {
 $item =null;
$value=null; 
$order = "id";
 $response=ProductsController::ctrShowProduct($item, $value, $order);
 echo json_encode($response);

	}else if($this->nameProduct != ""){
 $item ="description";
$value=$this->nameProduct;
$order = "id";

 $response=ProductsController::ctrShowProduct($item, $value,$order);
 echo json_encode($response);
	}else{

$item ="id";
$value=$this->idProduct;
$order = "id";

 $response=ProductsController::ctrShowProduct($item, $value, $order);
 echo json_encode($response);
}
}

}
// startObject instantiation...
if (isset($_POST["idCategory"])){
 $codeProduct = new AjaxProduct();	
 $codeProduct -> idCategory = $_POST["idCategory"];
 $codeProduct -> ajaxCreateCodeProduct();
}

//instantiate object for EditProduct

if (isset($_POST["idProduct"])) {
	$editProduct = new AjaxProduct();
	$editProduct -> idProduct=$_POST["idProduct"];
	$editProduct -> ajaxEditProduct();
}

//instantiate object for Sales small devices

if (isset($_POST["bringProduct"])) {
	$bringProduct = new AjaxProduct();
	$bringProduct -> bringProduct=$_POST["bringProduct"];
	$bringProduct -> ajaxEditProduct();
}

//instantiate object for Sales small devices  productname

if (isset($_POST["nameProduct"])) {
	$nameProduct = new AjaxProduct();
	$nameProduct -> nameProduct=$_POST["nameProduct"];
	$nameProduct -> ajaxEditProduct();
}
 ?>