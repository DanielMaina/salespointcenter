<?php 
require_once "connection.php";
/**
 * ProductModel
 */
class ProductsModel{
static public function mdlShowProduct($table, $item, $value,$order){
	if ($item != null) {
	$stmt = Connection::Connect()->prepare("SELECT * FROM $table WHERE $item=:$item ORDER BY $order DESC");
   $stmt->bindParam(":".$item, $value, PDO::PARAM_STR);
   $stmt->execute();
   return $stmt->fetch();
 	}else{
 	$stmt = Connection::Connect()->prepare("SELECT * FROM $table ORDER BY $order DESC");
 	$stmt-> execute();
 	return $stmt ->fetchAll();	
 	}
}
static public function mdlCreateProduct($table, $data){
	$stmt = Connection::Connect()->prepare("INSERT INTO $table(id_category,code,description,image,stock,buying_price,selling_price, sales) VALUES(:id_category, :code, :description, :image, :stock, :buying_price, :selling_price,:sales)");
	$stmt->bindParam(":id_category",$data["id_category"], PDO::PARAM_INT);
	$stmt->bindParam(":code",$data["code"], PDO::PARAM_STR);
	$stmt->bindParam(":description",$data["description"], PDO::PARAM_STR);
	$stmt->bindParam(":image",$data["image"], PDO::PARAM_STR);
	$stmt->bindParam(":stock",$data["stock"], PDO::PARAM_STR);
	$stmt->bindParam(":buying_price",$data["buying_price"], PDO::PARAM_STR);
	$stmt->bindParam(":selling_price",$data["selling_price"], PDO::PARAM_STR);
	$stmt->bindParam(":sales",$data["sales"], PDO::PARAM_STR);
	if ($stmt->execute()) {
		return "ok";
	}else{
		return "error";
	}
	
}

/*===================================
=            EDITPRODUCT            =
===================================*/
static public function mdlEditProduct($table, $data){
	$stmt = Connection::Connect()->prepare("UPDATE $table SET id_category=:id_category,description=:description,image=:image,stock=:stock,buying_price=:buying_price,selling_price=:selling_price, sales=:sales WHERE id =:id");
	$stmt->bindParam(":id_category",$data["id_category"]);
	$stmt->bindParam(":id",$data["id"]);
	$stmt->bindParam(":description",$data["description"]);
	$stmt->bindParam(":image",$data["image"]);
	$stmt->bindParam(":stock",$data["stock"]);
	$stmt->bindParam(":buying_price",$data["buying_price"]);
	$stmt->bindParam(":selling_price",$data["selling_price"]);
	$stmt->bindParam(":sales",$data["sales"]);
	if ($stmt->execute()) {
		return "ok";
	}else{
		return "error";
	}
	
}


/*======================================
=            DELETE PRODUCT            =
======================================*/
 static public function mdlDeleteProduct($table,$data){
 $stmt = Connection::Connect()->prepare("DELETE FROM $table WHERE id=:id ");
 $stmt -> bindParam(":id", $data, PDO::PARAM_INT);
 if($stmt -> execute()) {
 	return "ok";
 }else{
 	return "error";
 }
 }

	/*=============================================
	UPDATE PRODUCT
	=============================================*/

	static public function mdlUpdateProduct($table, $item1, $value1, $value){

		$stmt = Connection::Connect()->prepare("UPDATE $table SET $item1 = :$item1 WHERE id = :id");

		$stmt -> bindParam(":".$item1, $value1);
		$stmt -> bindParam(":id", $value);

		if($stmt -> execute()){

			return "ok";
		
		}else{

			return "error";	

		}

	} 


	/*=============================================
	SHOW ADDING OF THE SALES
	=============================================*/	

	static public function mdlShowAddingOfTheSales($table){

		$stmt = Connection::Connect()->prepare("SELECT SUM(sales) as total FROM $table");

		$stmt -> execute();

		return $stmt -> fetch();

		$stmt -> close();

		$stmt = null;
	}	

}//EndOfClass

 ?>