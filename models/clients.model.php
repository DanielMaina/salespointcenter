<?php 
require_once "connection.php";
/**
 * Handle client featurers
 */
class ModelClients{
	
static public function mdlCreateClient($table,$data){
$stmt = Connection::Connect()->prepare("INSERT INTO $table (name, document,email,phone,address, join_date) VALUES(:name, :document,:email,:phone,:address, :join_date)");

$stmt->bindParam(":name",$data["name"]);
$stmt->bindParam(":document",$data["documentId"]);
$stmt->bindParam(":email",$data["email"]);
$stmt->bindParam(":phone",$data["phone"]);
$stmt->bindParam(":address",$data["address"]);
$stmt->bindParam(":join_date",$data["join_date"]);

if ($stmt -> execute()) {
	return "ok";
}else{
	return "error";
}

}//ends method create user

/*====================================
=            SHOW CLIENTS            =
====================================*/


static public function mdlShowClient($table,$item,$value){
if($item !=null){
$stmt = Connection::Connect()->prepare("SELECT * FROM $table WHERE $item=:$item");
$stmt->bindParam(":".$item,$value, PDO::PARAM_STR);
$stmt->execute();
return $stmt->fetch();
}else{
 	$stmt = Connection::Connect()->prepare("SELECT * FROM $table");
 	$stmt-> execute();
 	return $stmt ->fetchAll();	
 	}

}

/*=========================================
=            EDIT CLIENT MODEL            =
=========================================*/

static public function mdlEditClient($table,$data){
$stmt = Connection::Connect()->prepare("UPDATE $table  SET name=:name,document=:document,email=:email,phone=:phone,address=:address, join_date=:join_date WHERE id=:id");

$stmt->bindParam(":id",$data["id"]);
$stmt->bindParam(":name",$data["name"]);
$stmt->bindParam(":document",$data["documentId"]);
$stmt->bindParam(":email",$data["email"]);
$stmt->bindParam(":phone",$data["phone"]);
$stmt->bindParam(":address",$data["address"]);
$stmt->bindParam(":join_date",$data["join_date"]);

if ($stmt -> execute()) {
	return "ok";
}else{
	return "error";
}

}

static public function mdlDeleteClient($table,$data){
$stmt = Connection::Connect()->prepare("DELETE FROM $table WHERE id=:id ");
 $stmt -> bindParam(":id", $data, PDO::PARAM_INT);
 if($stmt -> execute()) {
 	return "ok";
 }else{
 	return "error";
 }
}


	/*=============================================
	UPDATE CUSTOMER
	=============================================*/

	static public function mdlUpdateClient($table, $item1, $value1, $value){
		$stmt = Connection::Connect()->prepare("UPDATE $table SET $item1 = :$item1 WHERE id = :id");
		$stmt -> bindParam(":".$item1, $value1, PDO::PARAM_STR);
		$stmt -> bindParam(":id", $value, PDO::PARAM_STR);
		if($stmt -> execute()){
			return "ok";
		}else{
			return "error";	
		}
	}
  

}//ends class
 ?>