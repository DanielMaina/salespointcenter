<?php 
/*=============================================
=          CREATE THE MODEL FOR THE CATEGORY          =
=============================================*/
require_once "connection.php";
class CaterogyModel{
//METHOD TO ADD CATEGORY TO THE DB THROUGH INSERT METHOD 	
 static public function mdlCreateCategory($table, $data){
	$stmt = Connection::Connect()->prepare("INSERT INTO $table(category)VALUES (:category)");
	$stmt->bindParam(":category", $data, PDO::PARAM_STR);
	if ($stmt->execute()) {
		return "ok";
	}else{
		return "error";
	}
	// $stmt->close();
	// $stmt=null;
	}//end method 

//SHOW CATEGORY POPULATE:::
 static public function mdlShowCategory($table,$item,$value){
 	if($item != null){
   $stmt = Connection::Connect()->prepare("SELECT * FROM $table WHERE $item=:$item");
   $stmt->bindParam(":".$item, $value, PDO::PARAM_STR);
   $stmt->execute();
   return $stmt->fetch();
 	}else{
 	$stmt = Connection::Connect()->prepare("SELECT * FROM $table");
 	$stmt-> execute();
 	return $stmt ->fetchAll();	
 	}

 }
 //Moedel edit categories
 static public function mdlEditCategory($table, $data){
   $stmt = Connection::Connect()->prepare("UPDATE $table SET category=:category WHERE id=:id ");
	$stmt->bindParam(":id", $data["id"], PDO::PARAM_STR);
	$stmt->bindParam(":category", $data["category"], PDO::PARAM_STR);
	if ($stmt->execute()) {
		return "ok";
	}else{
		return "error";
	}
 }	

/*=============================================
=            DELETE CATEGORY MODEL            =
=============================================*/
static public function mdlDeleteCategory($table,$data){
$stmt = Connection::Connect()->prepare("DELETE FROM  $table WHERE id =:id ");
$stmt->bindParam(":id", $data, PDO::PARAM_STR);
if ($stmt -> execute()){
	return "ok";
}else{
	return "error";
}
}

}//end of the class

 ?>