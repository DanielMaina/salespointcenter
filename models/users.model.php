<?php 
require_once "connection.php";

/**
 * 
 */
class ModelUsers
{
	
	static public function mdlModelUsers ($table,$item,$value)
	{
	
	if ($item != null) {
	$stmt = Connection::Connect()->prepare("SELECT * FROM $table WHERE $item=:$item");
	$stmt->bindParam(":".$item, $value, PDO::PARAM_STR);
	$stmt->execute();
	return $stmt ->fetch();
  
  }else{
$stmt = Connection::Connect()->prepare("SELECT * FROM $table");
	$stmt -> execute();
	return $stmt -> fetchAll();
  }

	$stmt -> close();
	$stmt = null;
	}
	/*=============================================
	=           Register quana system user  Section            =
	========================== ===================*/

	static public function mdlAddUser($table, $data){
		$stmt = Connection::Connect()->prepare("INSERT INTO $table (name, username, password, profile, photo, status) VALUES (:name, :username, :password, :profile, :photo, :status)");
		$stmt->bindParam(":name", $data['name'], PDO::PARAM_STR);
		$stmt->bindParam(":username", $data['username'], PDO::PARAM_STR);
		$stmt->bindParam(":password", $data['password'], PDO::PARAM_STR);
		$stmt->bindParam(":profile", $data['profile'], PDO::PARAM_STR);
		$stmt->bindParam(":photo", $data['photo'], PDO::PARAM_STR);
		$stmt->bindParam(":status", $data['status'], PDO::PARAM_STR);

		if ($stmt->execute()) {
			return "ok";
		} else {
			return "error";
		}
 }
 /*=================================
 =          EDIT USER            =
 =================================*/
 
 static public function mdlEditUser($table, $data){
$stmt=Connection::Connect()->prepare("UPDATE $table SET name=:name,password=:password, profile=:profile, photo=:photo WHERE  username=:username");
$stmt->bindParam(":name", $data['name'], PDO::PARAM_STR);
$stmt->bindParam(":password", $data['password'], PDO::PARAM_STR);
$stmt->bindParam(":profile", $data['profile'], PDO::PARAM_STR);
$stmt->bindParam(":photo", $data['photo'], PDO::PARAM_STR);
$stmt->bindParam(":username", $data['username'], PDO::PARAM_STR);
if ($stmt->execute()) {
return "ok";
} else {
return "error";
}

 }
/*======================================
=            ACTIVATE USERS            =
======================================*/
static public function mdlUpdateUser($table,$item1,$value1,$item2,$value2){	
$stmt=Connection::Connect()->prepare("UPDATE $table SET $item1= :$item1 WHERE  $item2= :$item2");
$stmt->bindParam(":".$item1, $value1, PDO::PARAM_STR);
$stmt->bindParam(":".$item2, $value2, PDO::PARAM_STR);
if ($stmt -> execute()){
	return "ok";
}else{
	return "error";
}
}

/*====================================
=            DELETE USERS            =
====================================*/
static public function mdlDeleteUser($table,$data){
$stmt = Connection::Connect()->prepare("DELETE FROM $table WHERE id= :id");
$stmt->bindParam(":id",$data, PDO::PARAM_STR);	
if ($stmt -> execute()){
	return "ok";
}else{
	return "error";
}
}

}//endClass

 ?>