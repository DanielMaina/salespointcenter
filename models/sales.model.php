<?php 

/*=====================================
=            SLALES MODEL             =
=====================================*/
require_once "connection.php";
class SalesModel {
	
static public function mdlShowSales($table, $item,$value){
if ($item != null) {
	$stmt =Connection::Connect() -> prepare("SELECT * FROM $table WHERE $item = :$item ORDER BY id ASC ");
	$stmt -> bindParam(":".$item, $value);
	$stmt -> execute();
	return $stmt ->fetch();
}else{
	$stmt =Connection::Connect() -> prepare("SELECT * FROM $table ORDER BY id ASC");
	$stmt -> execute();
	return $stmt ->fetchAll();
}
}// end of saleModel


 static public function mdlAddSales($table, $data){

   $stmt = Connection::Connect()->prepare("INSERT INTO $table(code, id_client, id_seller, products, tax, net_price, total, payment_method) VALUES (:code, :idClient, :idSeller, :products, :tax, :netPrice, :totalPrice, :paymentMethod)");
        $stmt->bindParam(":code", $data["code"]);
		$stmt->bindParam(":idClient", $data["idClient"]);
		$stmt->bindParam(":idSeller", $data["idSeller"]);
		$stmt->bindParam(":products", $data["products"]);
		$stmt->bindParam(":tax", $data["tax"]);
		$stmt->bindParam(":netPrice", $data["netPrice"]);
		$stmt->bindParam(":totalPrice", $data["totalPrice"]);
		$stmt->bindParam(":paymentMethod", $data["paymentMethod"]);

		if($stmt->execute()){
			return "ok";
		}else{
			return "error";
		}
  }

//  mdlEditSales


 static public function mdlEditSales($table, $data){

   $stmt = Connection::Connect()->prepare("UPDATE $table SET id_client=:idClient, id_seller=:idSeller, products=:products, tax=:tax, net_price=:netPrice, total=:totalPrice, payment_method=:paymentMethod WHERE code=:code ");
        $stmt->bindParam(":code", $data["code"]);
		$stmt->bindParam(":idClient", $data["idClient"]);
		$stmt->bindParam(":idSeller", $data["idSeller"]);
		$stmt->bindParam(":products", $data["products"]);
		$stmt->bindParam(":tax", $data["tax"]);
		$stmt->bindParam(":netPrice", $data["netPrice"]);
		$stmt->bindParam(":totalPrice", $data["totalPrice"]);
		$stmt->bindParam(":paymentMethod", $data["paymentMethod"]);

		if($stmt->execute()){
			return "ok";
		}else{
			return "error";
		}
  }

  /*======================================
  =            DELETESALESFUNmdlDleteSeles            =
  ======================================*/
  static public function mdlDleteSele($table, $data){

		$stmt = Connection::Connect()->prepare("DELETE FROM $table WHERE id = :id");

		$stmt -> bindParam(":id", $data);

		if($stmt -> execute()){

			return "ok";
		
		}else{

			return "error";	

		}

	}
/*=============================================
	DATES RANGE
=============================================*/	

	static public function mdlSalesDatesRange($table, $initialDate, $finalDate){

		if($initialDate == null){

			$stmt = Connection::Connect()->prepare("SELECT * FROM $table ORDER BY id ASC");

			$stmt -> execute();

			return $stmt -> fetchAll();	


		}else if($initialDate == $finalDate){

			$stmt = Connection::Connect()->prepare("SELECT * FROM $table WHERE date like '%$finalDate%'");

			$stmt -> bindParam(":saledate", $finalDate, PDO::PARAM_STR);

			$stmt -> execute();

			return $stmt -> fetchAll();

		}else{

			$actualDate = new DateTime();
			$actualDate ->add(new DateInterval("P1D"));
			$actualDatePlusOne = $actualDate->format("Y-m-d");

			$finalDate2 = new DateTime($finalDate);
			$finalDate2 ->add(new DateInterval("P1D"));
			$finalDatePlusOne = $finalDate2->format("Y-m-d");

			if($finalDatePlusOne == $actualDatePlusOne){

				$stmt = Connection::Connect()->prepare("SELECT * FROM $table WHERE date BETWEEN '$initialDate' AND '$finalDatePlusOne'");

			}else{


				$stmt = Connection::Connect()->prepare("SELECT * FROM $table WHERE date BETWEEN '$initialDate' AND '$finalDate'");

			}
		
			$stmt -> execute();

			return $stmt -> fetchAll();

		}

	}

	/*===========================================
	=            mdlAddingTotalSales            =
	===========================================*/
static public function mdlAddingTotalSales($table){
$stmt = Connection::connect()->prepare("SELECT SUM(net_price) as total FROM $table");

		$stmt -> execute();

		return $stmt -> fetch();

		$stmt -> close();

		$stmt = null;
}	

	/*===========================================
	=            update debt accounts            =
	===========================================*/
	static public function mdlUpdateDebt($table, $data){
		$stmt=Connection::Connect()->prepare("INSERT INTO $table(receipt_no,payment_method,amount_paid,remark,pay_date) VALUES(:receipt_no,:payment_method,:amount_paid,:remark,:pay_date)");
		$stmt->bindParam(":receipt_no",$data["receipt_no"]);
		$stmt->bindParam(":payment_method",$data["payment_method"]);
		$stmt->bindParam(":amount_paid",$data["amount_paid"]);
		$stmt->bindParam(":remark",$data["remark"]);
		$stmt->bindParam(":pay_date",$data["pay_date"]);
	
		if ($stmt -> execute()) {
			return "ok";
		}else{
			return "error";
		}
	} //end of insert debt 
	/*===========================================
	=            see debt accounts            =
	===========================================*/
static public function mdlSeeDebt($table,$item,$value){
	if ($item != null) {
		$stmt = Connection::Connect()->prepare("SELECT * FROM $table WHERE $item=:$item");
		$stmt->bindParam(":".$item, $value, PDO::PARAM_STR);
		$stmt->execute();
		return $stmt ->fetchAll();
	  
	  }else{
	$stmt = Connection::Connect()->prepare("SELECT * FROM $table");
		$stmt -> execute();
		return $stmt -> fetchAll();
	  }
	
		$stmt -> close();
		$stmt = null;	
}

	/*===========================================
	=            Add expenses          =
	===========================================*/

static public function mdlPayExpenses($table,$data){
	$stmt=Connection::Connect()->prepare("INSERT INTO $table(amount,remark,pay_date) VALUES(:amount,:remark,:pay_date)");
	$stmt->bindParam(":amount",$data["amount"]);
	$stmt->bindParam(":remark",$data["remark"]);
	$stmt->bindParam(":pay_date",$data["pay_date"]);
	if($stmt -> execute()){
     return "ok";
	}else{
		return "error";
	}
}//end expenses

	/*===========================================
	=            see debt accounts            =
	===========================================*/
	static public function mdlSeeExpenses($table,$item,$value){
		if ($item != null) {
			$stmt = Connection::Connect()->prepare("SELECT * FROM $table WHERE $item=:$item");
			$stmt->bindParam(":".$item, $value, PDO::PARAM_STR);
			$stmt->execute();
			return $stmt ->fetchAll();
		  
		  }else{
		$stmt = Connection::Connect()->prepare("SELECT * FROM $table");
			$stmt -> execute();
			return $stmt -> fetchAll();
		  }
		
			$stmt -> close();
			$stmt = null;	
	}


}//end of class model 

 ?>